package net.crytec.regiongui;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * An enum for requesting strings from the language file.
 */

public enum Language {
  TITLE("title-name", "&2[&fRegionGUI&2]"),

  ERROR_WORLD_DISABLED("error.WorldDisabled", "&cSorry, but you can't claim a region in this world."),
  ERROR_NO_PERMISSION("error.noPermission", "&cSorry, but you don't have the required permission to do this."),
  ERROR_INVALID_OFFLINEPLAYER("error.offlineplayer", "&cThe entered player has never played on this server before."),
  ERROR_NO_REGION_FOUND("error.noRegionFound", "&cSorry, but there is no region on your current position."),
  ERROR_NOT_OWNER("error.notOwner", "&cSorry, but you do not own this region."),
  ERROR_NO_MONEY("error.notEnoughMoney", "&cYou don't have enough money to purchase this."),
  ERROR_OVERLAP("error.overlap", "&cYou would overlap one or more regions you don't own!"),
  ERROR_MAX_REGIONS("error.max_regions", "&cYou have reached the maximum amount of regions for this world."),
  ERROR_INVALID_NAME("error.invalidName", "&cThe entered region name contains invalid characters!"),
  ERROR_REGIONNAME_ALREADY_EXISTS("error.alreadyexists", "&cThere is already a region with this name."),
  ERROR_NO_HOME_SET("error.no-home", "&cNo teleport location found for the selected region. Please set a home position in the manage interface."),

  CHAT_ENTER_REGIONNAME("chat.enterRegionName", "&7Please enter a name of your region"),

  COMMAND_LIST_ENTRY("command.list.entry", "&7ID:&6 %region% &7 - Template: &6 %template%"),

  MANAGE_MEMBERADDED("manage.memberAdded", "&6%name%&7 is now a member of your region."),
  MANAGE_MEMBERREMOVED("manage.memberRemoved", "&6%name%&7 has been removed from your region."),

  INTERFACE_NEXT_PAGE("interface.general.nextpage", "&fNext Page"),
  INTERFACE_PREVIOUS_PAGE("interface.general.previous", "&fPrevious Page"),
  INTERFACE_BACK("interface.general.back", "&fBack"),

  INTERFACE_SELECT_TITLE("interface.select.title", "Select your Region"),
  INTERFACE_SELECT_DESCRIPTION("interface.select.description", "&7Left click to edit region &a%region%"),

  INTERFACE_MANAGE_TITLE("interface.manage.title", "&lManage your region"),
  INTERFACE_MANAGE_MEMBERS("interface.manage.members.name", "&6>> &7Current Members:"),
  INTERFACE_MANAGE_BUTTON_ADDMEMBER("interface.manage.button_addMember.name", "&7Click to &aadd&7 a player."),
  INTERFACE_MANAGE_BUTTON_INFO("interface.manage.button_info", "&6>> &7Info"),
  INTERFACE_MANAGE_BUTTON_INFO_DESCRIPTION("interface.manage.info.description", Arrays.asList("&6>>&7This is the default configuration", "&6>>&7 You can define a custom text which", "&6>>&7is shown in this menu :)")),


  INTERFACE_MANAGE_BUTTON_MANAGE_MEMBERS("interface.manage.button.managemembers", "&7Click to manage region members."),
  INTERFACE_MANAGE_BUTTON_DELETEREGION("interface.manage.button_deleteRegion.name", "&7Delete your region"),
  INTERFACE_MANAGE_BUTTON_DELETEREGION_DESCRIPTION("interface.manage.button_deleteRegion.description", Arrays.asList("&7This will delete your region", "&7there are &cno&7 refunds.")),
  INTERFACE_MANAGE_BUTTON_SHOWBORDER("interface.manage.button_showBorder.name", "&7Click to show the border."),
  INTERFACE_MANAGE_BUTTON_SHOWBORDER_DESCRIPTION("interface.manage.button_showBorder.description", Arrays.asList("&7This enables a particle border", "&7for a limited time.")),
  INTERFACE_MANAGE_BUTTON_FLAG("interface.manage.button_flag.name", "&7Click to open the Flag menu"),

  INTERFACE_REMOVE_TITLE("interface.remove.title", "Current Members"),
  INTERFACE_REMOVE_DESC("interface.remove.desc", "&7Click to remove %name%"),
  INTERFACE_REMOVE_SUCESSFULL("interface.remove.successfull", "&6%name% &7has been removed from your region."),

  INTERFACE_DELETE_TITLE("interface.delete.title", "&c&lConfirm Deletion"),
  INTERFACE_DELETE_CONFIRM_BUTTON("interface.delete.buttonConfirm", "&aConfirm"),
  INTERFACE_DELETE_ABORT_BUTTON("interface.delete.buttonAbort", "&cAbort"),
  INTERFACE_BUY_TITLE("interface.buy.title", "&f&lClaim a new region:"),

  INTERFACE_HOME_TITLE("interface.home.title", "&9Claimed Land"),
  INTERFACE_HOME_ENTRYBUTTON_DESCRIPTION("interface.home.entrybutton-description", Arrays.asList("&7World: %world%", "&aLeft click &7to teleport")),
  INTERFACE_HOME_BUTTON("interface.home.button", "&7Set home point"),
  INTERFACE_HOME_BUTTON_DESC("interface.home.buttondescription", Arrays.asList("&7Click to set the home teleport", "&7point at your current location")),
  INTERFACE_HOME_BUTTON_SUCCESS("interface.home.success", "&2The home teleport location has been set at your current position."),

  REGION_MESSAGE_CHATADDMEMBER("region.messageChatAddMember", "&7Please type the username you wish to add:"),
  REGION_REMOVED("region.removed", "&cYour region has been removed."),
  REGION_REMOVED_REFUNDED("region.refunded", "&7You have been refunded &a%refund%"),
  REGION_CREATE_SUCCESS("region.create", "&aA new Region has been created for you."),
  REGION_CREATE_MONEY("region.createMoney", "&aYou've paid %money% %currencyname%."),


  PREVIEW_CANCEL("region.preview.cancel", "&2Sneak to cancel border display"),

  FLAG_TITLE("flag.menu_title", "&lFlag Menu"),
  FLAG_CLEARED("flag.menu.cleared", "&7Flag (&6%flag%&7) has been cleared."),
  FLAG_ALLOWED("flag.menu.allowed", "&7Flag (&6%flag%&7) is now &2allowed!"),
  FLAG_DENIED("flag.menu.denied", "&7Flag (&6%flag%&7) is now &4forbidden"),
  FLAG_INPUT_CHAT("flag.message.chatinput", "&7Please enter the new value for flag %flag%:");


  private final String path;
  private String def;
  private boolean isArray = false;

  private List<String> defArray;
  private static YamlConfiguration LANG;

  Language(final String path, final String start) {
    this.path = path;
    def = start;
  }

  Language(final String path, final List<String> start) {
    this.path = path;
    defArray = start;
    isArray = true;
  }

  @Override
  public String toString() {
    return ChatColor.translateAlternateColorCodes('&', LANG.getString(path, def));
  }

  public String toChatString() {
    return TITLE.toString() + " " + ChatColor.translateAlternateColorCodes('&', LANG.getString(path, def));
  }

  public List<String> getDescriptionArray() {
    return LANG.getStringList(path).stream().map(x -> ChatColor.translateAlternateColorCodes('&', x)).collect(Collectors.toList());
  }

  public boolean isArray() {
    return isArray;
  }

  public List<String> getDefArray() {
    return defArray;
  }

  public String getDefault() {
    return def;
  }

  public String getPath() {
    return path;
  }

  private static boolean isValidPath(final String path) {
    return Arrays.stream(values()).anyMatch(lang -> lang.getPath().equals(path));
  }

  public static void initialize(final JavaPlugin plugin) throws IOException {
    final File languageFile = new File(plugin.getDataFolder(), "language.yml");
    if (!languageFile.exists() && !languageFile.createNewFile()) {
      plugin.getLogger().severe("Failed to create language.yml");
      return;
    }

    final YamlConfiguration langCfg = YamlConfiguration.loadConfiguration(languageFile);
    LANG = langCfg;

    int updated = 0;
    for (final Language entry : values()) {
      if (!langCfg.isSet(entry.getPath())) {
        langCfg.set(entry.getPath(), entry.isArray ? entry.getDefArray() : entry.getDefault());
        updated++;
      }
    }

    if (updated > 0) {
      langCfg.save(languageFile);
      plugin.getLogger().info("Updated language.yml with " + updated + " new entries!");
    }

    int removed = 0;
    for (final String key : langCfg.getRoot().getKeys(true)) {
      if (!langCfg.isConfigurationSection(key) && !isValidPath(key)) {
        plugin.getLogger().info(key + " is no longer a valid language translation...removing");
        langCfg.set(key, null);
        removed++;
      }
    }

    if (removed > 0) {
      plugin.getLogger().info("Removed " + removed + " old language entries from your language.yml");
      langCfg.save(languageFile);
    }
  }

}
