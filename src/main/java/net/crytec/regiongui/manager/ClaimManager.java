package net.crytec.regiongui.manager;

import com.google.common.collect.Maps;
import com.google.common.io.Files;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.data.RegionClaim;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;

public class ClaimManager implements Listener {

  private final RegionGUI plugin;
  private final PlayerManager playerManager;
  private final LinkedHashMap<UUID, RegionClaim> templates = Maps.newLinkedHashMap();
  private final File templateFolder;

  public ClaimManager(final RegionGUI plugin, final PlayerManager playerManager) {
    this.plugin = plugin;
    this.playerManager = playerManager;
    Bukkit.getPluginManager().registerEvents(this, plugin);

    final File templateFolder = new File(plugin.getDataFolder(), "templates");
    this.templateFolder = templateFolder;

    if (!this.templateFolder.exists() && !templateFolder.mkdirs()) {
      plugin.getLogger().warning("Failed to create Template folder!");
    }
  }

  public void registerTemplate(final RegionClaim claim) {
    templates.put(claim.getId(), claim);
  }

  public Set<RegionClaim> getTemplates(final World world) {
    return templates.values().stream().filter(claim -> claim.getWorld().isPresent()).filter(claim -> claim.getWorld().get().equals(world)).collect(Collectors.toSet());
  }

  public Optional<RegionClaim> getByName(final World world, final String name) {
    return templates.values().stream().filter(claim -> claim.getWorld().isPresent())
        .filter(claim -> claim.getWorld().get().equals(world) && claim.getDisplayname().equals(name)).findFirst();
  }

  public RegionClaim getClaimByID(final UUID uuid) {
    return templates.get(uuid);
  }

  public void deleteTemplate(final RegionClaim claim) {
    templates.remove(claim.getId());

    final File template = new File(templateFolder, claim.getId().toString() + ".claim");
    if (template.exists() && !template.delete()) {
      plugin.getLogger().severe("Failed to delete " + claim.getId().toString() + ".claim");
    }

    playerManager.getPlayerdata().values().forEach(claims -> claims.removeIf(ce -> ce.getTemplate().equals(claim)));
  }

  public void loadTemplates() {
    for (final File file : Files.fileTreeTraverser().breadthFirstTraversal(templateFolder).filter(f -> f.getName().endsWith(".claim"))) {
      try {
        final YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
        final RegionClaim claim = config.getSerializable("data", RegionClaim.class);
        templates.put(claim.getId(), claim);
      } catch (final Exception ex) {
        plugin.getLogger().severe("Failed to load template from file " + file.getName());
        ex.printStackTrace();
      }
    }
  }

  public void save() {
    for (final RegionClaim claim : templates.values()) {
      try {

        final File file = new File(templateFolder, claim.getId().toString() + ".claim");
        if (!file.exists()) {
          file.createNewFile();
        }
        final YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
        config.set("data", claim);
        config.save(file);

      } catch (final IOException ex) {
        plugin.getLogger().severe("Failed to save template to disk!");
      }
    }
  }
}