package net.crytec.regiongui.manager;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.LocationFlag;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import net.crytec.libs.commons.utils.lang.EnumUtils;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.data.flags.FlagSetting;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

public class FlagManager {

  private final TreeSet<FlagSetting> flags = Sets.newTreeSet();

  public FlagManager(final RegionGUI plugin) {
    final ArrayList<String> forbiddenFlags = Lists.newArrayList();
    forbiddenFlags.add("receive-chat");
    forbiddenFlags.add("allowed-cmds");
    forbiddenFlags.add("blocked-cmds");
    forbiddenFlags.add("send-chat");
    forbiddenFlags.add("invincible");
    forbiddenFlags.add("command-on-entry");
    forbiddenFlags.add("command-on-exit");
    forbiddenFlags.add("console-command-on-entry");
    forbiddenFlags.add("console-command-on-exit");
    forbiddenFlags.add("godmode");
    forbiddenFlags.add("worldedit");
    forbiddenFlags.add("chunk-unload");
    forbiddenFlags.add("passthrough");
    forbiddenFlags.add("price");

    final YamlConfiguration flagConfig = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "flags.yml"));
    boolean needSave = false;

    for (final Flag<?> flag : WorldGuard.getInstance().getFlagRegistry().getAll()) {
      if (forbiddenFlags.contains(flag.getName()) || flag instanceof LocationFlag)
        continue;

      final String path = "flags." + flag.getName() + ".";
      if (!flagConfig.isSet("flags." + flag.getName() + ".name")) {
        flagConfig.set(path + "name", "&7" + flag.getName());
        flagConfig.set(path + "enabled", true);
        flagConfig.set(path + "icon", Material.LIGHT_GRAY_DYE.toString());
        needSave = true;
      }

      Material icon = Material.BARRIER;

      if (EnumUtils.isValidEnum(Material.class, flagConfig.getString(path + "icon")))
        icon = Material.valueOf(flagConfig.getString(path + "icon"));

      if (flagConfig.getBoolean(path + "enabled"))
        addFlags(flag.getName(), flag, icon, ChatColor.translateAlternateColorCodes('&', flagConfig.getString(path + "name")));
    }
    if (needSave)
      try {
        flagConfig.save(new File(plugin.getDataFolder(), "flags.yml"));
      } catch (final IOException e) {
        e.printStackTrace();
      }

    final Permission allPerm = new Permission("region.flagmenu.all", "Allow the useage of all flags", PermissionDefault.FALSE);
    getActiveFlags().stream().map(FlagSetting::getPermission).collect(Collectors.toList()).forEach(perm -> {
      allPerm.getChildren().put(perm.getName(), perm.getDefault().getValue(false));
    });
  }


  private void addFlags(final String idenfifier, final Flag<?> flag, final Material icon, final String displayname) {
    flags.add(new FlagSetting(idenfifier, flag, icon, displayname));
  }

  public Set<FlagSetting> getActiveFlags() {
    return flags;
  }
}