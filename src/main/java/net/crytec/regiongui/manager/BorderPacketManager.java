package net.crytec.regiongui.manager;

import com.comphenix.protocol.wrappers.EnumWrappers.WorldBorderAction;
import net.crytec.libs.commons.utils.UtilLoc;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.data.RegionClaim;
import net.crytec.regiongui.util.packets.WrapperPlayServerWorldBorder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class BorderPacketManager {

  public BorderPacketManager(final RegionGUI plugin, final Location center, final RegionClaim claim) {
    this.plugin = plugin;

    this.center = UtilLoc.getCenter(center).clone();
    size = claim.getSize() + 1;
    centerPacket = new WrapperPlayServerWorldBorder();
    sizePacket = new WrapperPlayServerWorldBorder();
    resetcenterPacket = new WrapperPlayServerWorldBorder();
    resetsizePacket = new WrapperPlayServerWorldBorder();
    buildPackets();
  }

  private final RegionGUI plugin;

  private final Location center;
  private final int size;
  private final WrapperPlayServerWorldBorder centerPacket;
  private final WrapperPlayServerWorldBorder sizePacket;

  private final WrapperPlayServerWorldBorder resetcenterPacket;
  private final WrapperPlayServerWorldBorder resetsizePacket;

  private void buildPackets() {

    final World world = center.getWorld();

    resetsizePacket.setRadius(world.getWorldBorder().getSize());
    resetsizePacket.setOldRadius(world.getWorldBorder().getSize());
    resetsizePacket.setSpeed(0L);
    resetsizePacket.setAction(WorldBorderAction.LERP_SIZE);

    resetcenterPacket.setCenterX(world.getWorldBorder().getCenter().getBlockX());
    resetcenterPacket.setCenterZ(world.getWorldBorder().getCenter().getBlockZ());
    resetcenterPacket.setAction(WorldBorderAction.SET_CENTER);

    sizePacket.setRadius(size);
    sizePacket.setOldRadius(size);
    sizePacket.setSpeed(0L);
    sizePacket.setAction(WorldBorderAction.LERP_SIZE);

    centerPacket.setCenterX(center.getX());
    centerPacket.setCenterZ(center.getZ());
    centerPacket.setAction(WorldBorderAction.SET_CENTER);

  }

  public void sendReset(final Player player) {
    resetcenterPacket.sendPacket(player);
    Bukkit.getScheduler().runTaskLater(plugin, () -> resetsizePacket.sendPacket(player), 1L);
  }

  public void send(final Player player) {
    centerPacket.sendPacket(player);
    Bukkit.getScheduler().runTaskLater(plugin, () -> sizePacket.sendPacket(player), 1L);
  }

}
