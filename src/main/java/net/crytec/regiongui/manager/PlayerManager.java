package net.crytec.regiongui.manager;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.io.Files;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.Getter;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.data.ClaimEntry;
import net.crytec.regiongui.data.RegionClaim;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerManager implements Listener {

  private final RegionGUI plugin;
  @Getter
  private final HashMap<UUID, Set<ClaimEntry>> playerdata;
  private final File folder;

  public PlayerManager(final RegionGUI plugin, final ClaimManager manager) {
    this.plugin = plugin;
    playerdata = Maps.newHashMap();

    Bukkit.getPluginManager().registerEvents(this, plugin);

    folder = new File(plugin.getDataFolder(), "data");

    if (!folder.exists() && !folder.mkdirs()) {
      plugin.getLogger().warning("Failed to create data directory.");
    }
  }

  public void start() {
    // Workarround for reloads
    Bukkit.getOnlinePlayers().forEach(cur -> loadPlayerFiles(cur.getUniqueId()));

    for (final File file : Files.fileTreeTraverser().breadthFirstTraversal(folder).filter(file -> file.getName().endsWith(".data"))) {
      final UUID uuid = UUID.fromString(file.getName().replace(".data", ""));
      loadPlayerFiles(uuid);
    }
    plugin.getLogger().info("Loading " + playerdata.size() + " playerfiles...");
  }


  public void saveOnDisable() {
    Bukkit.getOnlinePlayers().forEach(cur -> {
      savePlayerFiles(cur.getUniqueId());
    });
    playerdata.clear();
  }

  public Set<ClaimEntry> getPlayerClaims(final UUID uuid) {
    if (playerdata.containsKey(uuid)) {
      return playerdata.get(uuid);
    } else {
      loadPlayerFiles(uuid);
    }
    return playerdata.get(uuid);
  }

  public Set<String> getRegionIDs(final Player player, final World world) {
    return playerdata.get(player.getUniqueId()).stream().map(ClaimEntry::getRegionID).collect(Collectors.toSet());
  }

  public void addClaimToPlayer(final UUID owner, final ProtectedRegion region, final RegionClaim claim) {
    playerdata.get(owner).add(new ClaimEntry(region.getId(), claim, System.currentTimeMillis()));
  }

  public void deleteClaim(final UUID owner, final ProtectedRegion region) {
    playerdata.get(owner).removeIf(claim -> claim.getProtectedRegion().get().equals(region));
    savePlayerFiles(owner);
  }

  public void loadPlayerFiles(final UUID uuid) {

    final File file = new File(folder, uuid.toString() + ".data");

    if (!file.exists()) {
      playerdata.put(uuid, Sets.newHashSet());
      return;
    }

    final YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

    final List<ClaimEntry> entries = (List<ClaimEntry>) config.getList("data");

    playerdata.put(uuid, Sets.newHashSet(entries));

    plugin.getLogger().info("Loaded " + playerdata.get(uuid).size() + " templates for player " + uuid.toString());

    //Check if worldguard regions and/or templates still exist.

    final Iterator<ClaimEntry> check = getPlayerClaims(uuid).iterator();

    while (check.hasNext()) {
      final ClaimEntry entry = check.next();

      if (entry == null) {
        plugin.getLogger().severe("Failed to load player claims for user: " + uuid.toString() + " (" + Bukkit.getOfflinePlayer(uuid).getName() + ")");
        continue;
      }

      if (entry.getTemplate() == null) {
        check.remove();
        plugin.getLogger().severe("Removed claim for player " + uuid.toString() + " because the given template does no longer exist.");
      } else if (entry.getProtectedRegion() == null || !entry.getProtectedRegion().isPresent()) {
        check.remove();
        plugin.getLogger().severe("Removed claim for player " + uuid.toString() + " because the WorldGuard region does no longer exist.");
      }
    }
  }

  public void savePlayerFiles(final UUID uuid) {
    if (playerdata.get(uuid).isEmpty()) {
      final File data = new File(folder, uuid.toString() + ".data");
      if (data.exists()) {
        data.delete();
      }
      return;
    }

    final File file = new File(folder, uuid.toString() + ".data");
    final YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

    final List<ClaimEntry> entries = Lists.newArrayList(playerdata.get(uuid));

    config.set("data", entries);

    try {
      config.save(file);
    } catch (final IOException e) {
      e.printStackTrace();
    }

  }

  @EventHandler
  public void onJoin(final PlayerJoinEvent event) {
    if (!playerdata.containsKey(event.getPlayer().getUniqueId())) {
      playerdata.put(event.getPlayer().getUniqueId(), Sets.newHashSet());
    }
  }

  @EventHandler
  public void onQuit(final PlayerQuitEvent event) {
    savePlayerFiles(event.getPlayer().getUniqueId());
  }

  public void saveImportedData() {
    playerdata.keySet().forEach(this::savePlayerFiles);
  }

}
