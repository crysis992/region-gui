package net.crytec.regiongui.events;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.crytec.regiongui.data.RegionClaim;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class RegionPurchasedEvent extends PlayerEvent {

  private static final HandlerList handlers = new HandlerList();

  private final ProtectedRegion region;
  private final RegionClaim claim;
  private final int price;

  public RegionPurchasedEvent(final Player who, final int price, final RegionClaim claim, final ProtectedRegion region) {
    super(who);
    this.region = region;
    this.claim = claim;
    this.price = price;
  }

  public ProtectedRegion getProtectedRegion() {
    return region;
  }

  public RegionClaim getRegionClaim() {
    return claim;
  }

  public int getPrice() {
    return price;
  }

  @Override
  public HandlerList getHandlers() {
    return handlers;
  }

  public static HandlerList getHandlerList() {
    return handlers;
  }
}