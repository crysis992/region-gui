package net.crytec.regiongui.events;

import net.crytec.regiongui.data.RegionClaim;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class RegionPrePurchaseEvent extends PlayerEvent implements Cancellable {

  private static final HandlerList handlers = new HandlerList();
  private boolean cancelled = false;

  private boolean generateBorder;
  private final RegionClaim claim;
  private int price;

  public RegionPrePurchaseEvent(final Player who, final int price, final RegionClaim claim, final boolean generateBorder) {
    super(who);
    this.generateBorder = generateBorder;
    this.claim = claim;
    this.price = price;
  }

  public boolean getGenerateBorder() {
    return generateBorder;
  }

  public RegionClaim getRegionClaim() {
    return claim;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(final int price) {
    this.price = price;
  }

  public void setGenerateBorder(final boolean value) {
    generateBorder = value;
  }

  @Override
  public HandlerList getHandlers() {
    return handlers;
  }

  public static HandlerList getHandlerList() {
    return handlers;
  }

  @Override
  public boolean isCancelled() {
    return cancelled;
  }

  @Override
  public void setCancelled(final boolean cancelled) {
    this.cancelled = cancelled;
  }
}