package net.crytec.regiongui.events;

import java.util.UUID;
import net.crytec.regiongui.data.RegionClaim;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class RegionAddMemberEvent extends PlayerEvent implements Cancellable {

  private static final HandlerList handlers = new HandlerList();
  private boolean cancelled = false;

  private final RegionClaim claim;
  private final UUID member;

  public RegionAddMemberEvent(final Player who, final RegionClaim claim, final UUID target) {
    super(who);
    this.claim = claim;
    member = target;
  }

  public RegionClaim getRegionClaim() {
    return claim;
  }

  public UUID getMember() {
    return member;
  }

  @Override
  public HandlerList getHandlers() {
    return handlers;
  }

  public static HandlerList getHandlerList() {
    return handlers;
  }

  @Override
  public boolean isCancelled() {
    return cancelled;
  }

  @Override
  public void setCancelled(final boolean cancelled) {
    this.cancelled = cancelled;
  }
}