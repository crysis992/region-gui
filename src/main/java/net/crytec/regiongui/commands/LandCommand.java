package net.crytec.regiongui.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.CommandIssuer;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Optional;
import co.aikar.commands.annotation.Subcommand;
import java.util.List;
import java.util.Set;
import net.crytec.inventoryapi.SmartInventory;
import net.crytec.libs.commons.utils.chat.ChatMessage;
import net.crytec.regiongui.Language;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.data.ClaimEntry;
import net.crytec.regiongui.manager.PlayerManager;
import net.crytec.regiongui.menus.LandBuyMenu;
import net.crytec.regiongui.menus.LandHomeMenu;
import net.crytec.regiongui.menus.RegionManageInterface;
import net.crytec.regiongui.menus.RegionSelectMenu;
import net.crytec.regiongui.util.PermissionRegistrar;
import net.crytec.regiongui.util.RegionUtils;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;


@CommandAlias("land")
public class LandCommand extends BaseCommand {

  private final PlayerManager manager;
  private final RegionGUI plugin;
  private final Permission claimPermission;

  public LandCommand(final RegionGUI plugin, final PlayerManager manager) {
    this.plugin = plugin;
    this.manager = manager;
    claimPermission = PermissionRegistrar.register("region.claim", "Required to claim regions with /land", PermissionDefault.TRUE);
  }

  @Default
  public void openLandInterface(final Player issuer) {
    if (!plugin.getConfig().getStringList("enabled_worlds").contains(issuer.getWorld().getName())) {
      issuer.sendMessage(Language.ERROR_WORLD_DISABLED.toChatString());
      return;
    }
    final List<ClaimEntry> regions = RegionUtils.getRegionsAtPosition(issuer);

    if (regions.isEmpty()) {
      if (issuer.hasPermission(claimPermission)) {
        SmartInventory.builder()
            .provider(new LandBuyMenu(plugin))
            .size(3, 9)
            .title(Language.INTERFACE_BUY_TITLE.toString())
            .build().open(issuer);
      } else {
        issuer.sendMessage(Language.ERROR_NO_PERMISSION.toChatString());
      }
    } else if (regions.size() == 1) {
      SmartInventory.builder().provider(new RegionManageInterface(regions.get(0))).size(3).title(Language.INTERFACE_MANAGE_TITLE.toString()).build().open(issuer);
    } else {
      SmartInventory.builder().provider(new RegionSelectMenu(regions)).size(3).title(Language.INTERFACE_SELECT_TITLE.toString()).build().open(issuer);
    }
  }

  @Subcommand("help")
  public void sendCommandHelp(final CommandIssuer issuer, final CommandHelp help) {
    help.showHelp(issuer);
  }

  @Subcommand("home")
  public void openHomeGUI(final Player issuer) {
    SmartInventory.builder().id("home-" + issuer.getName()).provider(new LandHomeMenu()).size(5, 9).title(Language.INTERFACE_HOME_TITLE.toString()).build().open(issuer);
  }

  @Subcommand("list")
  @CommandPermission("region.list")
  public void displayLandList(final Player issuer, @Optional final OfflinePlayer op) {
    if (op == null) {

      final Set<ClaimEntry> claims = RegionGUI.getInstance().getPlayerManager().getPlayerClaims(issuer.getUniqueId());

      for (final ClaimEntry entry : claims) {
        final String template = entry.getTemplate().getDisplayname();
        final String region = entry.getRegionID();
        issuer.sendMessage(Language.COMMAND_LIST_ENTRY.toChatString().replace("%region%", region).replace("%template%", template));
      }
    } else {
      if (!op.hasPlayedBefore()) {
        issuer.sendMessage(Language.ERROR_INVALID_OFFLINEPLAYER.toChatString());
        return;
      }
      if (!issuer.hasPermission("region.list.others")) {
        issuer.sendMessage(Language.ERROR_NO_PERMISSION.toChatString());
        return;
      }
      final Set<ClaimEntry> claims = RegionGUI.getInstance().getPlayerManager().getPlayerClaims(op.getUniqueId());

      //TODO Test Implementation
      for (final ClaimEntry entry : claims) {
        final String template = entry.getTemplate().getDisplayname();
        final String region = entry.getRegionID();

        final ChatMessage msg = new ChatMessage(Language.COMMAND_LIST_ENTRY.toChatString().replace("%region%", region).replace("%template%", template));
        msg.appendClickHandler("[§2Teleport§r]", null, (clicker, args) -> {
          clicker.sendMessage("Teleporting to region..." + region);
        });

        msg.send(issuer);

//        final BaseComponent component = new TextComponent(Language.COMMAND_LIST_ENTRY.toChatString().replace("%region%", region).replace("%template%", template));
//
//        final TextComponent teleportButton = new TextComponent("[Teleport]");
//        teleportButton.setClickEvent(new ClickEvent(Action.RUN_COMMAND, ""));
//
//        component.addExtra(teleportButton);
//        issuer.spigot().sendMessage(component);

//        issuer.sendMessage(Language.COMMAND_LIST_ENTRY.toChatString().replace("%region%", region).replace("%template%", template));
      }
    }
  }
}