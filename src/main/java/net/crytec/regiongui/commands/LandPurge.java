package net.crytec.regiongui.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import com.google.common.collect.Maps;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.UUID;
import net.crytec.libs.commons.utils.UtilTime;
import net.crytec.regiongui.RegionGUI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

@CommandAlias("land purge")
@CommandPermission("region.admin.purge")
public class LandPurge extends BaseCommand {

  private final RegionGUI plugin;
  private final HashMap<UUID, Integer> confirmation;

  public LandPurge(final RegionGUI instance) {
    plugin = instance;
    confirmation = Maps.newHashMap();
  }

  @Default
  public void purgeRegions(final Player issuer, final int months) {

    int amount = 0;

    for (final OfflinePlayer op : Bukkit.getOfflinePlayers()) {
      final long lastOnlineTime = op.getLastPlayed();
      final long purgeTime = UtilTime.localDateToInstant(LocalDateTime.now().minusMonths(months)).getEpochSecond();

      if (lastOnlineTime < purgeTime && plugin.getPlayerManager().getPlayerClaims(op.getUniqueId()).size() > 0) {
        amount++;
      }
    }

    issuer.sendMessage(ChatColor.RED + "Purging regions of inactive players.. (" + months + " Months) ");
    issuer.sendMessage(ChatColor.RED + "" + amount + " Players will be purged.");
    issuer.sendMessage(ChatColor.RED + "To start purging type " + ChatColor.DARK_RED + "/land purge confirm");
    issuer.sendMessage("");
    issuer.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "!! THIS FEATURE IS STILL WORK IN PROGRESS (experimental)!! ");


  }

  @CommandAlias("confirm")
  public void confirmCommand(final Player issuer) {

    if (!confirmation.containsKey(issuer.getUniqueId())) {
      issuer.sendMessage(ChatColor.RED + "You need to specify a time limit in months. /land purge <months>");
      return;
    }

    final int months = confirmation.get(issuer.getUniqueId());
    for (final OfflinePlayer op : Bukkit.getOfflinePlayers()) {
      final long lastOnlineTime = op.getLastPlayed();
      final long purgeTime = UtilTime.localDateToInstant(LocalDateTime.now().minusMonths(months)).getEpochSecond();

      if (lastOnlineTime < purgeTime && plugin.getPlayerManager().getPlayerClaims(op.getUniqueId()).size() > 0) {
        issuer.sendMessage(op.getName() + " will be purged due to offline limit.");

        plugin.getPlayerManager().getPlayerClaims(op.getUniqueId()).forEach(claim -> {
          if (!claim.getProtectedRegion().isPresent()) {
            plugin.getLogger().warning("ProtectedRegion not found for Player " + op.getUniqueId() + " skipping purge..");
            return;
          }
          final ProtectedRegion region = claim.getProtectedRegion().get();
          plugin.getPlayerManager().deleteClaim(op.getUniqueId(), region);
          issuer.sendMessage("Region '" + region.getId() + "' of Player " + op.getName() + " was purged.");
        });
      }
    }
    confirmation.remove(issuer.getUniqueId());
  }

}