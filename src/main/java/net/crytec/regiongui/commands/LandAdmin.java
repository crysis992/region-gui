package net.crytec.regiongui.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.CommandIssuer;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Description;
import co.aikar.commands.annotation.Subcommand;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.crytec.inventoryapi.SmartInventory;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.data.RegionClaim;
import net.crytec.regiongui.menus.admin.AdminTemplateList;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

@CommandAlias("landadmin")
@CommandPermission("region.admin")
public class LandAdmin extends BaseCommand {

  public LandAdmin(final RegionGUI plugin) {
    this.plugin = plugin;
  }

  private final RegionGUI plugin;

  @Default
  public void openEditor(final Player player) {
    SmartInventory.builder().provider(new AdminTemplateList()).size(6).title("Template Editor [" + player.getWorld().getName() + "]").build().open(player);
  }

  @Subcommand("editor")
  @Description("Opens the land template editor")
  public void landAdminCommand(final Player player) {
    SmartInventory.builder().provider(new AdminTemplateList()).size(6).title("Template Editor [" + player.getWorld().getName() + "]").build().open(player);
  }

  @Subcommand("addregion")
  @Description("Adds a defined worldguard region to a player with a given template, regardless of size or checks. This is a forced action.")
  public void forceAdd(final Player player, final OfflinePlayer owner, final ProtectedRegion region, final RegionClaim claim) {
    plugin.getPlayerManager().addClaimToPlayer(owner.getUniqueId(), region, claim);
    player.sendMessage(ChatColor.GRAY + "Assigned region to player. Please note this is a forced action and no size/permission check apply.");
  }

  @Subcommand("help")
  public void sendCommandHelp(final CommandIssuer issuer, final CommandHelp help) {
    help.showHelp(issuer);
  }

}
