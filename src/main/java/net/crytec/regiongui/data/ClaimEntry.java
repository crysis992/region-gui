package net.crytec.regiongui.data;

import com.google.common.collect.Maps;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import net.crytec.regiongui.RegionGUI;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;


@SerializableAs("PlayerClaim")
public class ClaimEntry implements ConfigurationSerializable {

  final String regionID;
  final RegionClaim template;
  final long timestamp;

  private ProtectedRegion protectedRegion;

  public ClaimEntry(final String regionID, final RegionClaim claim, final long timestamp) {
    this.regionID = regionID;
    template = claim;
    this.timestamp = timestamp;

    if (!template.getWorld().isPresent()) {
      Bukkit.getLogger().severe("Failed to load template - World does not exist!");
      return;
    }

    final World world = template.getWorld().get();
    final RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
    final RegionManager regionManager = container.get(BukkitAdapter.adapt(world));

    if (regionManager == null) {
      throw new IllegalStateException("Unable to create ClaimEntry - WorldGuard is disabled for World " + world.getName());
    }

    if (!regionManager.hasRegion(this.regionID)) {
      RegionGUI.getInstance().getLogger().info("Region " + regionID + " in world " + template.getWorld() + " does no longer exist!");
      return;
    }
    protectedRegion = regionManager.getRegion(this.regionID);
  }

  public Optional<ProtectedRegion> getProtectedRegion() {
    if (protectedRegion != null) {
      return Optional.of(protectedRegion);
    }

    if (!template.getWorld().isPresent()) {
      Bukkit.getLogger().severe("Failed to parse Region " + regionID + "  - World is no longer loaded!");
      return Optional.empty();
    }
    final World world = template.getWorld().get();

    final ProtectedRegion region = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(world)).getRegion(regionID);

    if (region == null) {
      RegionGUI.getInstance().getLogger().info("Region " + regionID + " in world " + world.getName() + " does no longer exist!");
      return Optional.empty();
    } else {
      protectedRegion = region;
      return Optional.of(region);
    }
  }

  public String getRegionID() {
    return regionID;
  }

  public RegionClaim getTemplate() {
    return template;
  }

  public long getTimestamp() {
    return timestamp;
  }

  @Override
  public Map<String, Object> serialize() {
    final Map<String, Object> map = Maps.newHashMap();

    map.put("template", template.getId().toString());
    map.put("timestamp", timestamp);
    map.put("region", regionID);

    return map;
  }

  public static ClaimEntry deserialize(final Map<String, Object> map) {
    final String regionID = (String) map.get("region");
    final UUID template = UUID.fromString(((String) map.get("template")));
    final long timestamp = (Long) map.get("timestamp");

    final RegionClaim claim = RegionGUI.getInstance().getClaimManager().getClaimByID(template);

    if (claim == null) {
      RegionGUI.getInstance().getLogger().severe("Failed to load claim! Template does no longer exist!");
      return null;
    }

    return new ClaimEntry(regionID, claim, timestamp);
  }
}