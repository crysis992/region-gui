package net.crytec.regiongui.data;

import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.crytec.libs.commons.utils.item.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.inventory.ItemStack;

@ToString
@SerializableAs("Template")
public class RegionClaim implements ConfigurationSerializable, Comparable<RegionClaim> {

  @Getter
  private final UUID id;

  // GUI
  @Getter
  @Setter
  private String displayname = "Default displayname";
  @Setter
  private Material icon = Material.BARRIER;
  @Getter
  @Setter
  private List<String> description = Collections.singletonList(ChatColor.GRAY + "Default description");

  // Data
  @Getter
  @Setter
  private int size = 10;
  @Setter
  private String world;
  @Getter
  @Setter
  private int height = 256;
  @Getter
  @Setter
  private int depth = 256;
  @Getter
  @Setter
  private int price = -1;
  @Getter
  @Setter
  private int refund = 0;

  @Getter
  @Setter
  private Material borderMaterial = Material.OAK_FENCE;
  @Getter
  @Setter
  private boolean generateBorder = true;

  @Getter
  @Setter
  private List<String> runCommands = new ArrayList<>();

  @Getter
  @Setter
  private String permission = "";

  @Getter
  private List<String> noPermDescription = Collections.singletonList("Upgrade your rank to purchase this template.");

  public RegionClaim(final World world) {
    id = UUID.randomUUID();
    size = 10;
    this.world = world.getName();
  }

  private RegionClaim(final UUID id) {
    this.id = id;
  }

  public ItemStack getIcon() {
    return new ItemBuilder(icon).name(displayname).build();
  }

  public void setNoPermDescription(final List<String> list) {
    noPermDescription = list.stream().map(x -> ChatColor.translateAlternateColorCodes('&', x)).collect(Collectors.toList());
  }

  public boolean hasRefund() {
    return refund > 0;
  }

  public Optional<World> getWorld() {
    return Optional.ofNullable(Bukkit.getWorld(world));
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof RegionClaim)) {
      return false;
    }
    final RegionClaim other = (RegionClaim) obj;
    return Objects.equals(id, other.id);
  }

  @Override
  public Map<String, Object> serialize() {
    final Map<String, Object> map = Maps.newHashMap();

    map.put("id", id.toString());

    map.put("gui.displayname", displayname);
    map.put("gui.icon", icon.toString());
    map.put("gui.description", description);
    map.put("gui.noPermDescription", noPermDescription);

    map.put("data.size", size);
    map.put("data.heigth", height);
    map.put("data.depth", depth);
    map.put("data.price", price);
    map.put("data.refund", refund);
    map.put("data.world", world);
    map.put("data.permission", permission);

    map.put("border.material", borderMaterial.toString());
    map.put("border.enabled", generateBorder);

    map.put("generation.runCommands", runCommands);

    return map;
  }


  public static RegionClaim deserialize(final Map<String, Object> map) {
    final RegionClaim claim = new RegionClaim(UUID.fromString((String) map.get("id")));

    claim.setDisplayname((String) map.get("gui.displayname"));
    claim.setIcon(Material.valueOf((String) map.get("gui.icon")));
    claim.setDescription((List<String>) map.get("gui.description"));
    claim.setNoPermDescription((List<String>) map.get("gui.noPermDescription"));

    claim.setSize((int) map.get("data.size"));
    claim.setHeight((int) map.get("data.heigth"));
    claim.setDepth((int) map.get("data.depth"));
    claim.setPrice((int) map.get("data.price"));
    claim.setRefund((int) map.get("data.refund"));
    claim.setWorld((String) map.get("data.world"));
    claim.setPermission((String) map.getOrDefault("data.permission", ""));

    claim.setBorderMaterial(Material.valueOf((String) map.get("border.material")));
    claim.setGenerateBorder((Boolean) map.get("border.enabled"));
    claim.setRunCommands((List<String>) map.get("generation.runCommands"));

    Bukkit.getServer().getLogger().info("Loaded Template ID " + claim.getId() + " - " + claim.getDisplayname());

    return claim;
  }

  @Override
  public int compareTo(final RegionClaim o) {
    return Integer.compare(size, o.getSize());
  }
}