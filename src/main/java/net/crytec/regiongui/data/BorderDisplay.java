package net.crytec.regiongui.data;

import com.comphenix.protocol.wrappers.EnumWrappers.WorldBorderAction;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.regions.CuboidRegion;
import net.crytec.regiongui.Language;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.util.packets.WrapperPlayServerWorldBorder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

public class BorderDisplay implements Runnable {

  private final WrapperPlayServerWorldBorder resetcenterPacket;
  private final WrapperPlayServerWorldBorder resetsizePacket;

  private final WrapperPlayServerWorldBorder centerPacket;
  private final WrapperPlayServerWorldBorder sizePacket;

  private final Player player;
  private final int size;

  private final BukkitTask task;

  public BorderDisplay(final Player player, final ClaimEntry claim) {
    this.player = player;
    final World world = player.getWorld();
		size = claim.getTemplate().getSize();

    // Build reset packets
		resetcenterPacket = new WrapperPlayServerWorldBorder();
		resetsizePacket = new WrapperPlayServerWorldBorder();

		resetsizePacket.setRadius(world.getWorldBorder().getSize());
		resetsizePacket.setOldRadius(world.getWorldBorder().getSize());
		resetsizePacket.setSpeed(0L);
		resetsizePacket.setAction(WorldBorderAction.LERP_SIZE);

    final CuboidRegion region = new CuboidRegion(claim.getProtectedRegion().get().getMinimumPoint(), claim.getProtectedRegion().get().getMaximumPoint());

		resetcenterPacket.setCenterX(world.getWorldBorder().getCenter().getBlockX());
		resetcenterPacket.setCenterZ(world.getWorldBorder().getCenter().getBlockZ());
		resetcenterPacket.setAction(WorldBorderAction.SET_CENTER);

		centerPacket = new WrapperPlayServerWorldBorder();
		sizePacket = new WrapperPlayServerWorldBorder();

		sizePacket.setRadius(size);
		sizePacket.setOldRadius(size);
		sizePacket.setSpeed(0L);
		sizePacket.setAction(WorldBorderAction.LERP_SIZE);

    final Location center = BukkitAdapter.adapt(player.getWorld(), region.getCenter()).clone().add(0.5, 0, 0.5);

		centerPacket.setCenterX(center.getX());
		centerPacket.setCenterZ(center.getZ());
		centerPacket.setAction(WorldBorderAction.SET_CENTER);

		centerPacket.sendPacket(this.player);
    Bukkit.getScheduler().runTaskLater(RegionGUI.getInstance(), () -> sizePacket.sendPacket(this.player), 1L);

		task = Bukkit.getScheduler().runTaskTimer(RegionGUI.getInstance(), this, 5, 20);
  }

  public void reset() {
		resetcenterPacket.sendPacket(player);
    Bukkit.getScheduler().runTaskLater(RegionGUI.getInstance(), () -> resetsizePacket.sendPacket(player), 1L);
  }

  @Override
  public void run() {
    if (!player.isOnline() || player.isSneaking()) {
			task.cancel();
			reset();
			player.resetTitle();
      return;
    }
		player.sendTitle("", Language.PREVIEW_CANCEL.toString(), 0, 30, 0);
  }
}