package net.crytec.regiongui.data.flags;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.extension.platform.Actor;
import com.sk89q.worldguard.protection.flags.BooleanFlag;
import com.sk89q.worldguard.protection.flags.DoubleFlag;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.FlagContext;
import com.sk89q.worldguard.protection.flags.IntegerFlag;
import com.sk89q.worldguard.protection.flags.InvalidFlagFormat;
import com.sk89q.worldguard.protection.flags.SetFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.StringFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import java.util.stream.Collectors;
import lombok.Getter;
import net.crytec.inventoryapi.api.ClickableItem;
import net.crytec.inventoryapi.api.InventoryContent;
import net.crytec.libs.commons.utils.item.ItemBuilder;
import net.crytec.regiongui.Language;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.util.PlayerChatInput;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;


public class FlagSetting implements Comparable<FlagSetting> {

  private final Flag<?> flag;
  private final String id;
  private Material icon = Material.PAPER;
  private final FlagInputType inputType;
  @Getter
  private final Permission permission;
  private final String displayname;

  public FlagSetting(final String id, final Flag<?> flag, final Material icon, final String displayname) {
    this.flag = flag;
    this.id = id;
    this.displayname = displayname;
    this.icon = icon;

    permission = new Permission("region.flagmenu." + this.id, "Enables the use of the " + id + " flag.", PermissionDefault.TRUE);
    try {
      Bukkit.getPluginManager().addPermission(permission);
    } catch (final IllegalArgumentException ignored) {
    }

    if (flag instanceof StringFlag)
      inputType = FlagInputType.STRING;
    else if (flag instanceof StateFlag)
      inputType = FlagInputType.STATE;
    else if (flag instanceof SetFlag)
      inputType = FlagInputType.SET;
    else if (flag instanceof IntegerFlag)
      inputType = FlagInputType.INTEGER;
    else if (flag instanceof BooleanFlag)
      inputType = FlagInputType.BOOLEAN;
    else
      inputType = FlagInputType.UNKNOWN;
  }

  public ClickableItem getButton(final Player player, final ProtectedRegion region, final InventoryContent contents) {

    final ItemBuilder builder = new ItemBuilder(icon).name(getName());
    builder.setItemFlag(ItemFlag.HIDE_ATTRIBUTES);
    builder.setItemFlag(ItemFlag.HIDE_ENCHANTS);

    if (region.getFlags().containsKey(getFlag())) {

      builder.enchantment(Enchantment.ARROW_INFINITE);

      if (inputType == FlagInputType.STATE) {
        final StateFlag sf = (StateFlag) getFlag();
        final String name = (region.getFlag(sf) == StateFlag.State.DENY) ? ChatColor.RED + getName() : ChatColor.DARK_GREEN + getName();
        builder.name(name);
      }
    }
    builder.lore("");
    builder.lore(getCurrentValue(region));

    return new ClickableItem(builder.build(), e -> {
      if (e.getClick() == ClickType.RIGHT && region.getFlags().containsKey(getFlag())) {
        region.setFlag(getFlag(), null);
        player.sendMessage(Language.FLAG_CLEARED.toString().replace("%flag%", getName()));
        contents.getHost().getProvider().reopen(player, contents);
        return;
      }

      //Boolean or state
      if (inputType == FlagInputType.STATE || inputType == FlagInputType.BOOLEAN) {
        switchState(player, region);
        contents.getHost().getProvider().reopen(player, contents);

      } else {
        // Everything else requires user input
        player.closeInventory();
        player.sendMessage(Language.FLAG_INPUT_CHAT.toChatString().replace("%flag%", getName()));
        PlayerChatInput.get(player, input -> {
          try {
            setFlag(region, flag, BukkitAdapter.adapt(player), input);
            Bukkit.getScheduler().runTaskLater(RegionGUI.getInstance(), () -> contents.getHost().getProvider().reopen(player, contents), 1L);
          } catch (final InvalidFlagFormat e1) {
            player.sendMessage(e1.getMessage());
          }

        });

      }
    });
  }


  private void switchState(final Player player, final ProtectedRegion region) {

    if (inputType == FlagInputType.STATE) {
      final StateFlag sf = (StateFlag) getFlag();

      if (region.getFlags().containsKey(getFlag()))
        if (region.getFlag(sf) == StateFlag.State.DENY) {
          region.setFlag(sf, StateFlag.State.ALLOW);
          player.sendMessage(Language.FLAG_ALLOWED.toString().replace("%flag%", getName()));
        } else {
          region.setFlag(sf, StateFlag.State.DENY);
          player.sendMessage(Language.FLAG_DENIED.toString().replace("%flag%", getName()));
        }
      else {
        region.setFlag(sf, StateFlag.State.ALLOW);
        player.sendMessage(Language.FLAG_ALLOWED.toString().replace("%flag%", getName()));
      }
    } else {
      final BooleanFlag bf = (BooleanFlag) getFlag();

      if (region.getFlags().containsKey(getFlag()))
        if (!region.getFlag(bf).booleanValue()) {
          region.setFlag(bf, true);
          player.sendMessage(Language.FLAG_ALLOWED.toString().replace("%flag%", getName()));
        } else {
          region.setFlag(bf, false);
          player.sendMessage(Language.FLAG_DENIED.toString().replace("%flag%", getName()));
        }
      else {
        region.setFlag(bf, true);
        player.sendMessage(Language.FLAG_ALLOWED.toString().replace("%flag%", getName()));
      }

    }
  }

  public String getCurrentValue(final ProtectedRegion region) {

    if (!region.getFlags().containsKey(getFlag()))
      return ChatColor.RED + "Flag not set";

    switch (inputType) {
      case BOOLEAN:
        return region.getFlag(((BooleanFlag) getFlag())) ? "Yes" : "No";
      case DOUBLE:
        return "" + region.getFlag(((DoubleFlag) getFlag())).doubleValue();
      case INTEGER:
        return "" + region.getFlag(((IntegerFlag) getFlag())).intValue();
      case SET: {
        final SetFlag<String> setflag = (SetFlag<String>) getFlag();
        return region.getFlag(setflag).stream().collect(Collectors.joining(",", "[", "]"));
      }
      case STATE:
        return (region.getFlag(((StateFlag) getFlag())) == StateFlag.State.DENY) ? "false" : "true";
      case STRING:
        return region.getFlag(((StringFlag) getFlag())).toString();
      case UNKNOWN:
        return ChatColor.GRAY + "Unknown";
      default:
        return "Unable to query flag value";

    }
  }


  public void setIcon(final Material mat) {
    icon = mat;
  }

  public Material getIcon() {
    return icon;
  }

  public String getId() {
    return id;
  }

  public Flag<?> getFlag() {
    return flag;
  }

  protected static <V> void setFlag(final ProtectedRegion region, final Flag<V> flag, final Actor sender, final String value) throws InvalidFlagFormat {
    region.setFlag(flag, flag.parseInput(FlagContext.create().setSender(sender).setInput(value).setObject("region", region).build()));
  }

  public String getName() {
    return displayname;
  }

  @Override
  public int compareTo(final FlagSetting other) {
    return getId().compareTo(other.getId());
  }
}