package net.crytec.regiongui.data.flags;

public enum FlagInputType {

  SET,
  STATE,
  DOUBLE,
  INTEGER,
  BOOLEAN,
  STRING,
  UNKNOWN
}
