package net.crytec.regiongui;

import co.aikar.commands.BukkitCommandManager;
import co.aikar.commands.InvalidCommandArgument;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import lombok.Getter;
import net.crytec.inventoryapi.InventoryAPI;
import net.crytec.libs.commons.utils.CommonsAPI;
import net.crytec.regiongui.commands.LandAdmin;
import net.crytec.regiongui.commands.LandCommand;
import net.crytec.regiongui.commands.LandPurge;
import net.crytec.regiongui.data.ClaimEntry;
import net.crytec.regiongui.data.RegionClaim;
import net.crytec.regiongui.listener.InternalListeners;
import net.crytec.regiongui.manager.ClaimManager;
import net.crytec.regiongui.manager.FlagManager;
import net.crytec.regiongui.manager.PlayerManager;
import net.crytec.regiongui.util.PlayerChatInput;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerLoadEvent;
import org.bukkit.event.server.ServerLoadEvent.LoadType;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public final class RegionGUI extends JavaPlugin implements Listener {

  // ---------------------- [ Instance for Singleton ] ----------------------
  @Getter
  private static RegionGUI instance;

  @Getter
  private Economy economy;

  @Getter
  private FlagManager flagManager;
  @Getter
  private ClaimManager claimManager;
  @Getter
  private PlayerManager playerManager;

  public static boolean reloaded = false;

  @Override
  public void onLoad() {
    instance = this;
    ConfigurationSerialization.registerClass(RegionClaim.class, "Template");
    ConfigurationSerialization.registerClass(ClaimEntry.class, "PlayerClaim");

    if (!getDataFolder().exists() && !getDataFolder().mkdir()) {
      getLogger().warning("Failed to create plugin directory!");
    }

    final File config = new File(getDataFolder(), "config.yml");
    try {
      if (!config.exists()) {
        config.createNewFile();
        saveResource("config.yml", true);
      }
    } catch (final Exception ex) {
      ex.printStackTrace();
    }

  }

  @Override
  public void onEnable() {
    if (reloaded) {
      Bukkit.getPluginManager().disablePlugin(this);
      getLogger().severe("Reloadig the plugin is not supported, please restart your server!");
      return;
    }
    if (!setupEconomy()) {
      getLogger().severe("Unable to load RegionGUI - No Economy Plugin found.");
      Bukkit.getPluginManager().disablePlugin(this);
      return;
    }

    try {
      Language.initialize(this);
    } catch (final IOException e) {
      getLogger().severe("Failed to initialize language file, please report this error to the author!");
      e.printStackTrace();
    }

    Bukkit.getPluginManager().registerEvents(this, this);

    new PlayerChatInput(this);
    new InventoryAPI(this);
    final CommonsAPI commonsAPI = new CommonsAPI(this);
    commonsAPI.setHostPrefix(Language.TITLE.toString());

    flagManager = new FlagManager(this);
    playerManager = new PlayerManager(this, claimManager);
    claimManager = new ClaimManager(this, playerManager);

    claimManager.loadTemplates();
    playerManager.start();

    final BukkitCommandManager commandManager = new BukkitCommandManager(this);
    setupCommands(commandManager);
    commandManager.enableUnstableAPI("help");
    commandManager.registerCommand(new LandCommand(this, playerManager));
    commandManager.registerCommand(new LandAdmin(this));
    commandManager.registerCommand(new LandPurge(this));

    Bukkit.getPluginManager().registerEvents(new InternalListeners(this), this);

    final Metrics metrics = new Metrics(this);
    metrics.addCustomChart(new Metrics.SimplePie("purges_enabled", () -> getConfig().getBoolean("deleteOnPurge") ? "Enabled" : "Disabled"));
    metrics.addCustomChart(new Metrics.SimplePie("preview_mode", () -> getConfig().getBoolean("enable_previewmode") ? "Enabled" : "Disabled"));
    metrics.addCustomChart(new Metrics.SimplePie("economy", () -> (economy.getName() != null) ? economy.getName() : "Unknown"));
    reloaded = true;
  }

  @Override
  public void onDisable() {
    if (getPlayerManager() != null) {
      getPlayerManager().saveOnDisable();
    }
    if (claimManager != null) {
      claimManager.save();
    }

    ConfigurationSerialization.unregisterClass(ClaimEntry.class);
    ConfigurationSerialization.unregisterClass(RegionClaim.class);
  }

  private void setupCommands(final BukkitCommandManager manager) {
    manager.getCommandContexts().registerContext(ProtectedRegion.class, c -> {
      final World world = c.getPlayer().getWorld();
      final String arg = c.popFirstArg();

      final ProtectedRegion region = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(world)).getRegion(arg);
      if (region == null) {
        throw new InvalidCommandArgument("Unable to find a WorldGuard region with id " + arg + " in your current world.");
      } else {
        return region;
      }
    });

    manager.getCommandContexts().registerContext(RegionClaim.class, c -> {
      final World world = c.getIssuer().getPlayer().getWorld();
      final String arg = c.popFirstArg();

      final Optional<RegionClaim> claim = getClaimManager().getTemplates(world)
          .stream()
          .filter(rc -> ChatColor.stripColor(rc.getDisplayname()).equals(arg)).findFirst();

      if (!claim.isPresent()) {
        throw new InvalidCommandArgument("Could not find a template with that name in your current world.");
      } else {
        return claim.get();
      }
    });

  }

  @EventHandler
  public void preventReload(final ServerLoadEvent event) {
    if (event.getType() == LoadType.RELOAD) {
      Bukkit.getPluginManager().disablePlugin(this);
      getLogger().severe("== UNABLE TO LOAD PLUGIN RegionGUI ==");
      getLogger().severe("Please do not use /reload as this is not supported by this plugin!");
    }
  }

  private boolean setupEconomy() {
    if (getServer().getPluginManager().getPlugin("Vault") == null) {
      return false;
    }
    final RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
    if (rsp == null) {
      return false;
    }
    economy = rsp.getProvider();
    return true;
  }
}