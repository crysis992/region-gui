package net.crytec.regiongui.menus.admin;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import net.crytec.inventoryapi.SmartInventory;
import net.crytec.inventoryapi.api.ClickableItem;
import net.crytec.inventoryapi.api.InventoryContent;
import net.crytec.inventoryapi.api.InventoryProvider;
import net.crytec.inventoryapi.api.SlotPos;
import net.crytec.libs.commons.utils.UtilMath;
import net.crytec.libs.commons.utils.UtilPlayer;
import net.crytec.libs.commons.utils.chatinput.ChatInput;
import net.crytec.libs.commons.utils.item.ItemBuilder;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.data.RegionClaim;
import net.crytec.regiongui.util.PlayerChatInput;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

public class TemplateEditor implements InventoryProvider {

  private static final ItemStack filler = new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").build();

  public TemplateEditor(final RegionClaim claim) {
    this.claim = claim;
  }

  private final RegionClaim claim;

  @Override
  public void init(final Player player, final InventoryContent contents) {

    contents.fillRow(0, ClickableItem.empty(filler));
    contents.fillRow(4, ClickableItem.empty(filler));

    contents.set(SlotPos.of(0, 4), ClickableItem.of(new ItemBuilder(claim.getIcon()).name(ChatColor.GRAY + "Set list icon")
        .lore(ChatColor.GRAY + "Click with an Item on your")
        .lore(ChatColor.GRAY + "curser to set the list icon")
        .build(), e -> {
      if (e.getClick() == ClickType.LEFT && e.getCursor() != null && e.getCursor().getType() != Material.AIR) {
        UtilPlayer.playSound(player, Sound.UI_BUTTON_CLICK, 0.5F, 1F);
        claim.setIcon(e.getCursor().getType());
        e.getView().getBottomInventory().addItem(e.getCursor());
        e.getView().setCursor(new ItemStack(Material.AIR));
        reopen(player, contents);
      }
    }));

    contents.set(SlotPos.of(1, 0), ClickableItem.of(new ItemBuilder(Material.NAME_TAG).name(ChatColor.GRAY + "Template")
        .lore(ChatColor.GRAY + "Current name: " + ChatColor.GOLD + claim.getDisplayname()).build(), e -> {

      player.closeInventory();
      player.sendMessage(ChatColor.GRAY + "Please enter the new " + ChatColor.GOLD + "displayname: ");
      UtilPlayer.playSound(player, Sound.UI_BUTTON_CLICK, 0.5F, 1F);
      PlayerChatInput.get(player, input -> {
        claim.setDisplayname(input);
        SmartInventory.builder().provider(new AdminTemplateList()).size(6).title("Template Editor [" + player.getWorld().getName() + "]").build().open(player);
      });


    }));

    contents.set(SlotPos.of(2, 0), ClickableItem.of(new ItemBuilder(Material.BOOK).name(ChatColor.GRAY + "Description")
            .lore(ChatColor.GRAY + "Current description:")
            .lore(claim.getDescription().stream().map(line -> ChatColor.translateAlternateColorCodes('&', line)).collect(Collectors.toList()))
            .lore("")
            .lore(ChatColor.GREEN + "Left click " + ChatColor.GRAY + "to add a new line")
            .lore(ChatColor.GREEN + "Right click " + ChatColor.GRAY + "to delete the last line.")
            .build()
        , e -> {
          if (e.getClick() == ClickType.RIGHT) {
            if (claim.getDescription().size() <= 0) {
              return;
            }

            final List<String> description = claim.getDescription();
            description.remove(description.size() - 1);
            claim.setDescription(description);
            player.sendMessage(ChatColor.RED + "Line removed.");
            reopen(player, contents);
            return;
          }

          player.closeInventory();
          player.sendMessage(ChatColor.GRAY + "Enter a new line. Type 'exit' to abort");

          new ChatInput(player, ChatColor.GRAY + "Enter a new line. Type 'exit' to abort", true, input -> {
            if (input.equals("exit")) {
              reopen(player, contents);
              return;
            }

            final List<String> description = claim.getDescription() == null ? new ArrayList<>() : claim.getDescription();
            description.add(input);
            claim.setDescription(description);
            reopen(player, contents);
            player.sendMessage(ChatColor.GREEN + "Line added.");
          });
        }));

    contents.set(SlotPos.of(1, 1), ClickableItem.of(new ItemBuilder(Material.BLAZE_POWDER).name(ChatColor.GRAY + "Set Permission")
        .lore(ChatColor.GRAY + "Current Permission: " + ChatColor.GOLD + claim.getPermission()).build(), event -> {
      player.closeInventory();
      UtilPlayer.playSound(player, Sound.UI_BUTTON_CLICK, 0.5F, 1F);
      player.sendMessage(ChatColor.GRAY + "Please enter the new " + ChatColor.GOLD + "permission node " + ChatColor.GRAY + "into the chat:");
      PlayerChatInput.get(player, input -> {
        claim.setPermission(input);
        reopen(player, contents);
      });
    }));

    contents.set(SlotPos.of(2, 1), ClickableItem.of(new ItemBuilder(Material.REDSTONE_TORCH).name(ChatColor.GRAY + "Set 'No Permission' description")
            .lore(ChatColor.YELLOW + "This lines will be added below")
            .lore(ChatColor.YELLOW + "the claim description in /land")
            .lore("")
            .lore(ChatColor.GRAY + "Current:")
            .lore(claim.getNoPermDescription())
            .lore(ChatColor.GRAY + "Left click to add a new line")
            .lore(ChatColor.GRAY + "Right click to delete the last line.")
            .build()
        , e -> {
          if (e.getClick() == ClickType.RIGHT) {
            if (claim.getDescription().size() <= 0) {
              return;
            }
            claim.getNoPermDescription().remove(claim.getNoPermDescription().size() - 1);
            reopen(player, contents);
            return;
          }

          player.closeInventory();
          player.sendMessage(ChatColor.GRAY + "Enter a new line. Type 'exit' to abort");

          PlayerChatInput.get(player, input -> {
            if (input.equals("exit")) {
              Bukkit.getScheduler().runTask(RegionGUI.getInstance(), () -> reopen(player, contents));
              return;
            }
            claim.getNoPermDescription().add(ChatColor.translateAlternateColorCodes('&', input));

            Bukkit.getScheduler().runTask(RegionGUI.getInstance(), () -> reopen(player, contents));
          });
        }));

    contents.set(SlotPos.of(1, 2), ClickableItem.of(new ItemBuilder(Material.GOLD_NUGGET).name(ChatColor.GRAY + "Price")
        .lore(ChatColor.GRAY + "Current Price: " + ChatColor.GOLD + +claim.getPrice()).build(), event -> {

      player.closeInventory();
      player.sendMessage(ChatColor.GRAY + "Please enter the new " + ChatColor.GOLD + "price" + ChatColor.GRAY + " into the chat:");
      PlayerChatInput.get(player, input -> {

        if (!UtilMath.isInt(input)) {
          player.sendMessage(ChatColor.DARK_RED + "Error: The given input is not a valid integer!");
          reopen(player, contents);
          return;
        }

        UtilPlayer.playSound(player, Sound.UI_BUTTON_CLICK, 0.5F, 1F);
        claim.setPrice(Integer.parseInt(input));
        reopen(player, contents);

      });
    }));

    contents.set(SlotPos.of(2, 2), ClickableItem.of(new ItemBuilder(Material.GOLD_NUGGET).name(ChatColor.GRAY + "Refund")
        .lore(ChatColor.GRAY + "This amount will be added to")
        .lore(ChatColor.GRAY + "the players balance on deletion")
        .lore(ChatColor.GRAY + "Current Refund: " + ChatColor.GOLD + +claim.getRefund()).build(), event -> {

      player.closeInventory();
      player.sendMessage(ChatColor.GRAY + "Please enter the new " + ChatColor.GOLD + "refund amount" + ChatColor.GRAY + " into the chat:");
      PlayerChatInput.get(player, input -> {

        if (!UtilMath.isInt(input)) {
          player.sendMessage(ChatColor.RED + "Error: The given input is not a valid integer!");
          reopen(player, contents);
          return;
        }
        UtilPlayer.playSound(player, Sound.UI_BUTTON_CLICK, 0.5F, 1F);
        claim.setRefund(Integer.parseInt(input));
        reopen(player, contents);
      });

    }));

    contents.set(SlotPos.of(1, 3), ClickableItem.of(new ItemBuilder(Material.BEACON).name(ChatColor.GRAY + "Set Size")
        .lore(ChatColor.GRAY + "Current Size: " + ChatColor.GOLD + claim.getSize())
        .lore(ChatColor.GRAY + "Increasing the size will not update")
        .lore(ChatColor.GRAY + "already claimed/existing regions.")
        .lore(ChatColor.GRAY + "This does only affect new regions")
        .build(), event -> {

      player.closeInventory();
      player.sendMessage(ChatColor.GRAY + "Please enter the new " + ChatColor.GOLD + "size" + ChatColor.GRAY + " into the chat:");
      PlayerChatInput.get(player, input -> {

        if (!UtilMath.isInt(input)) {
          player.sendMessage(ChatColor.RED + "Error: The given input is not a valid integer!");
          reopen(player, contents);
          return;
        }

        UtilPlayer.playSound(player, Sound.UI_BUTTON_CLICK, 0.5F, 1F);
        claim.setSize(Integer.parseInt(input));
        reopen(player, contents);
      });
    }));

    contents.set(SlotPos.of(1, 5), new ClickableItem(new ItemBuilder(Material.COMMAND_BLOCK).name(ChatColor.GRAY + "Commands")
        .lore(ChatColor.GRAY + "This is a set of commands that will be")
        .lore(ChatColor.GRAY + "executed by the 'player' after")
        .lore(ChatColor.GRAY + "a successfull purchase.")
        .lore(ChatColor.GRAY + "You may use this to set default flags or")
        .lore(ChatColor.GRAY + "whatever you want.")
        .lore(ChatColor.GRAY + "Valid placeholders:")
        .lore(ChatColor.YELLOW + "%player%" + ChatColor.GRAY + " - The players name")
        .lore(ChatColor.YELLOW + "%region%" + ChatColor.GRAY + " - The purchased region")
        .lore(ChatColor.YELLOW + "%world%" + ChatColor.GRAY + " - The worldname")
        .lore("")
        .lore(ChatColor.GRAY + "To run a command from the console,")
        .lore(ChatColor.GRAY + "simply put" + ChatColor.YELLOW + "<server>" + ChatColor.GRAY + " in front")
        .lore("")
        .lore(ChatColor.GRAY + "Right click to " + ChatColor.RED + "delete" + ChatColor.GRAY + " the last command in the list.")

        .lore(ChatColor.GRAY + "Current Commands:")
        .lore(claim.getRunCommands())
        .build(), event -> {
      UtilPlayer.playSound(player, Sound.UI_BUTTON_CLICK, 0.5F, 1F);

      if (event.isRightClick()) {
        if (claim.getRunCommands().size() == 0) {
          return;
        }

        claim.getRunCommands().remove(claim.getRunCommands().size() - 1);
        reopen(player, contents);
        return;
      }

      player.closeInventory();
      player.sendMessage(ChatColor.GRAY + "Please enter the command you want to add (without the first slash (/)): ");
      player.sendMessage(ChatColor.GRAY + "(Start it with <server> to execute it from the console. You may use following placeholders: %player% %world% %region%)");
      PlayerChatInput.get(player, input -> {
        claim.getRunCommands().add(input);
        reopen(player, contents);
      });

    }));

    contents.set(SlotPos.of(1, 4), ClickableItem.of(new ItemBuilder(Material.OAK_FENCE).name(ChatColor.GRAY + "Enable Border")
        .lore(ChatColor.GRAY + "Currently enabled: " + (claim.isGenerateBorder() ? "yes" : "no"))
        .build(), e -> {
      UtilPlayer.playSound(player, Sound.UI_BUTTON_CLICK, 0.5F, 1F);
      claim.setGenerateBorder(!claim.isGenerateBorder());
      reopen(player, contents);

    }));

    //TODO Implement a maximum member feature
//		contents.set(SlotPos.of(1, 5), ClickableItem.of(new ItemBuilder(Material.PLAYER_HEAD).name(ChatColor.GRAY + "Set Maximum Members")
//				.lore(ChatColor.GRAY + "Current value: " + ChatColor.GOLD + + claim.getMaxMembers())
//				.build()
//				, e -> {
//					if (e.getClick() == ClickType.LEFT) {
//						this.claim.setMaxMembers(this.claim.getMaxMembers() +1);
//						UtilPlayer.playSound(player, Sound.UI_BUTTON_CLICK, 0.5F, 1F);
//						this.reopen(player, contents);
//					} else if (e.getClick() == ClickType.RIGHT && this.claim.getMaxMembers() >= 1) {
//						this.claim.setMaxMembers(this.claim.getMaxMembers() -1);
//						UtilPlayer.playSound(player, Sound.UI_BUTTON_CLICK, 0.5F, 1F);
//						this.reopen(player, contents);
//					}
//				}));

    contents.set(SlotPos.of(2, 4), ClickableItem.of(new ItemBuilder(claim.getBorderMaterial()).name(ChatColor.GRAY + "Set Border Material")
        .lore(ChatColor.GRAY + "Click with an Item on your")
        .lore(ChatColor.GRAY + "curser to set the border material")
        .build(), e -> {
      if (e.getClick() == ClickType.LEFT && e.getCursor() != null && e.getCursor().getType() != Material.AIR) {

        if (!e.getCursor().getType().isBlock()) {
          player.sendMessage(ChatColor.DARK_RED + "ERROR: The given material is not a placeable block.");
          reopen(player, contents);
          return;
        }

        UtilPlayer.playSound(player, Sound.UI_BUTTON_CLICK, 0.5F, 1F);
        claim.setBorderMaterial(e.getCursor().getType());
        e.getView().getBottomInventory().addItem(e.getCursor());
        e.getView().setCursor(new ItemStack(Material.AIR));
        reopen(player, contents);
      }
    }));

    // Delete Template
    contents.set(SlotPos.of(4, 8), ClickableItem.of(new ItemBuilder(Material.TNT).name(ChatColor.DARK_RED + "Delete template")
        .lore(ChatColor.GRAY + "Deleting this template will remove it")
        .lore(ChatColor.GRAY + "from all players that have already")
        .lore(ChatColor.GRAY + " it.")
        .build(), e ->

    {
      player.closeInventory();
      player.sendMessage(ChatColor.DARK_RED + "Type 'confirm' to confirm the deletion of the selected template:");
      PlayerChatInput.get(player, input -> {
        if (input.equals("confirm")) {
          UtilPlayer.playSound(player, Sound.ENTITY_GENERIC_EXPLODE, 0.5F, 1.15F);
          RegionGUI.getInstance().getClaimManager().deleteTemplate(claim);
          reopen(player, contents);
        } else {
          reopen(player, contents);
          UtilPlayer.playSound(player, Sound.ENTITY_LEASH_KNOT_PLACE, 1, 0.85F);
        }
      });
    }));

    // Save Template
    contents.set(SlotPos.of(4, 4), ClickableItem.of(new ItemBuilder(Material.EMERALD).name(ChatColor.DARK_GREEN + "Save template").build(), e -> {
      RegionGUI.getInstance().getClaimManager().save();
      SmartInventory.builder().provider(new AdminTemplateList()).size(6).title("Template Editor [" + player.getWorld().getName() + "]").build().open(player);
    }));
  }
}