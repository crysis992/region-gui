package net.crytec.regiongui.menus.admin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.crytec.inventoryapi.SmartInventory;
import net.crytec.inventoryapi.api.ClickableItem;
import net.crytec.inventoryapi.api.InventoryContent;
import net.crytec.inventoryapi.api.InventoryProvider;
import net.crytec.inventoryapi.api.SlotPos;
import net.crytec.libs.commons.utils.UtilPlayer;
import net.crytec.libs.commons.utils.item.ItemBuilder;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.data.RegionClaim;
import net.crytec.regiongui.manager.ClaimManager;
import net.crytec.regiongui.util.PlayerChatInput;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class AdminTemplateList implements InventoryProvider {

  private final ClaimManager claimManager;

  public AdminTemplateList() {
    claimManager = RegionGUI.getInstance().getClaimManager();
  }

  @Override
  public void init(final Player player, final InventoryContent contents) {

    final World world = player.getWorld();

    final ArrayList<RegionClaim> claims = new ArrayList<>(claimManager.getTemplates(world));
    Collections.sort(claims);

    for (final RegionClaim claim : claims) {

      final ItemBuilder builder = new ItemBuilder((claim.getIcon() == null ? Material.BARRIER : claim.getIcon().getType()));
      builder.name(ChatColor.translateAlternateColorCodes('&', claim.getDisplayname()));

      final List<String> lore = new ArrayList<>(claim.getDescription());
      lore.replaceAll(line -> ChatColor.translateAlternateColorCodes('&', line));
      builder.lore(lore);

      contents.add(ClickableItem.of(builder.build(), e -> {
        UtilPlayer.playSound(player, Sound.UI_BUTTON_CLICK, 0.5F, 1F);
        SmartInventory.builder().provider(new TemplateEditor(claim)).size(5).title("Editing " + claim.getDisplayname()).build().open(player);
      }));
    }

    contents.set(SlotPos.of(5, 4), new ClickableItem(new ItemBuilder(Material.EMERALD).name(ChatColor.GREEN + "Create new template").build(), e -> {
      UtilPlayer.playSound(player, Sound.UI_BUTTON_CLICK, 0.5F, 1F);
      player.closeInventory();
      player.sendMessage(ChatColor.GRAY + "Please enter the name for this template:");
      PlayerChatInput.get(player, input -> {
        final RegionClaim claim = new RegionClaim(player.getWorld());
        claimManager.registerTemplate(claim);
        claimManager.save();
        reopen(player, contents);
      });
    }));
  }
}