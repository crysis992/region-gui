package net.crytec.regiongui.menus;

import com.google.common.collect.Lists;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.crytec.inventoryapi.api.ClickableItem;
import net.crytec.inventoryapi.api.InventoryContent;
import net.crytec.inventoryapi.api.InventoryProvider;
import net.crytec.libs.commons.utils.UtilMath;
import net.crytec.libs.commons.utils.item.ItemBuilder;
import net.crytec.regiongui.Language;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.data.RegionClaim;
import net.crytec.regiongui.data.RegionPreview;
import net.crytec.regiongui.events.RegionPrePurchaseEvent;
import net.crytec.regiongui.manager.ClaimManager;
import net.crytec.regiongui.util.PlayerChatInput;
import net.crytec.regiongui.util.PlotBuilder;
import net.crytec.regiongui.util.RegionUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.permissions.PermissionAttachmentInfo;

public class LandBuyMenu implements InventoryProvider {

  private final ClaimManager manager;
  private final RegionGUI plugin;

  public LandBuyMenu(final RegionGUI plugin) {
    this.plugin = plugin;
    manager = plugin.getClaimManager();
  }

  @Override
  public void init(final Player player, final InventoryContent content) {
    final List<RegionClaim> claims = Lists.newArrayList(manager.getTemplates(player.getWorld()));

    if (claims.isEmpty()) {
      player.closeInventory();
      return;
    }

    Collections.sort(claims);

    if (claims.isEmpty()) {
      player.closeInventory();
      return;
    }

    int max_regions = 2;

    final long ownedRegions = RegionGUI.getInstance().getPlayerManager().getPlayerClaims(player.getUniqueId()).stream()
        .filter(claim -> {
          if (claim.getTemplate().getWorld().isPresent()) {
            return claim.getTemplate().getWorld().get().getName().equals(player.getWorld().getName());
          } else {
            return false;
          }
        })
        .count();

    for (final PermissionAttachmentInfo perms : player.getEffectivePermissions()) {
      // Get the max value should there be more than one
      if (perms.getPermission().startsWith("region.maxregions.")) {
        if (UtilMath.isInt(perms.getPermission().split("region.maxregions.")[1])) {
          max_regions = Math.max(max_regions, Integer.valueOf(perms.getPermission().split("region.maxregions.")[1]));
        } else {
          max_regions = 0;
          RegionGUI.getInstance().getLogger().severe("Failed to parse permission node [" + perms.getPermission().split("region.maxregions.")[1] + "]");
          RegionGUI.getInstance().getLogger().severe("[ERROR] Make sure the last entry is a valid number!");
        }
      }
      // Do some sanity checking
      if (max_regions < 0) {
        max_regions = 0;
      }
    }

    if (ownedRegions >= max_regions) {
      player.sendMessage(Language.ERROR_MAX_REGIONS.toString());
      player.closeInventory();
      return;
    }

    int i = 0;
    for (final RegionClaim claim : claims) {

      final ItemBuilder builder = new ItemBuilder(claim.getIcon().clone());
      builder.name(ChatColor.translateAlternateColorCodes('&', claim.getDisplayname()));
      builder.setItemFlag(ItemFlag.HIDE_ATTRIBUTES);

      final int max = max_regions;
      final List<String> lore = new ArrayList<>(claim.getDescription());
      lore.replaceAll(line -> line.replaceAll("%current%", String.valueOf(ownedRegions)));
      lore.replaceAll(line -> line.replaceAll("%max_claims%", String.valueOf(max)));
      lore.replaceAll(line -> line.replaceAll("%size%", String.valueOf(claim.getSize())));
      lore.replaceAll(line -> line.replaceAll("%depth%", String.valueOf(claim.getDepth())));
      lore.replaceAll(line -> line.replaceAll("%height%", String.valueOf(claim.getHeight())));
      lore.replaceAll(line -> ChatColor.translateAlternateColorCodes('&', line));
      builder.lore(lore);

      if (!claim.getPermission().isEmpty() && !player.hasPermission(claim.getPermission())) {
        builder.lore("");
        builder.lore(claim.getNoPermDescription());
        content.set(0, i, ClickableItem.empty(builder.build()));
      } else {
        content.add(ClickableItem.of(builder.build(), e -> {

          if (e.getClick() == ClickType.LEFT) {

            final RegionPrePurchaseEvent event = new RegionPrePurchaseEvent(player, claim.getPrice(), claim, claim.isGenerateBorder());
            Bukkit.getPluginManager().callEvent(event);

            if (event.isCancelled()) {
              return;
            }

            player.closeInventory();
            player.sendMessage(Language.CHAT_ENTER_REGIONNAME.toChatString());

            PlayerChatInput.get(player, name -> {

              if (!name.matches("^[a-zA-Z0-9\\-_]*$")) {
                player.sendMessage(Language.ERROR_INVALID_NAME.toChatString());
                return;
              }
              final String identifier = plugin.getConfig().getString("region-identifier").replace("%player%", player.getName()).replace("%displayname%", name);
              final ProtectedRegion region = RegionUtils.getRegionManager(player.getWorld()).getRegion(identifier);
              if (region != null) {
                player.sendMessage(Language.ERROR_REGIONNAME_ALREADY_EXISTS.toChatString());
                return;
              } else {
                final PlotBuilder plotBuilder = new PlotBuilder(player, name, claim);
                plotBuilder.build();
                player.resetTitle();
              }
            });
          } else if (e.getClick() == ClickType.RIGHT) {
            new RegionPreview(player, claim.getSize() + 1);
            player.closeInventory();
          }

        }));
      }
      i++;
    }
  }
}