package net.crytec.regiongui.menus;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;
import net.crytec.inventoryapi.SmartInventory;
import net.crytec.inventoryapi.api.ClickableItem;
import net.crytec.inventoryapi.api.InventoryContent;
import net.crytec.inventoryapi.api.InventoryProvider;
import net.crytec.libs.commons.utils.item.ItemBuilder;
import net.crytec.regiongui.Language;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.data.BorderDisplay;
import net.crytec.regiongui.data.ClaimEntry;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

public class RegionManageInterface implements InventoryProvider {

  private static final ItemStack fill = new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").build();
  private final ClaimEntry claim;

  public RegionManageInterface(final ClaimEntry claim) {
    this.claim = claim;
  }

  @Override
  public void init(final Player player, final InventoryContent contents) {

    contents.fillBorders(ClickableItem.empty(fill));

    if (!claim.getProtectedRegion().isPresent()) {
      RegionGUI.getInstance().getLogger().severe("Failed to open /land interface - WorldGuard Region does no longer exist.");
      player.closeInventory();
      return;
    }

    final ProtectedRegion region = claim.getProtectedRegion().get();

    final Iterator<UUID> member = region.getMembers().getUniqueIds().iterator();
    final ArrayList<String> lore = new ArrayList<>();
    lore.add(Language.INTERFACE_MANAGE_MEMBERS.toString());

    while (member.hasNext()) {
      final OfflinePlayer op = Bukkit.getOfflinePlayer(member.next());
      if (op.hasPlayedBefore())
        lore.add(ChatColor.GOLD + op.getName());
    }

    if (player.hasPermission("region.manage.members"))
      contents.set(1, 1, ClickableItem.of(new ItemBuilder(Material.PLAYER_HEAD).name(Language.INTERFACE_MANAGE_BUTTON_MANAGE_MEMBERS.toString()).build(), e -> {
        SmartInventory.builder().id("regiongui.deletemember")
            .provider(new RegionManageMember(claim))
            .size(5)
            .title(Language.INTERFACE_REMOVE_TITLE.toString())
            .build().open(player);
      }));

    contents.set(1, 3, ClickableItem.of(new ItemBuilder(Material.ENDER_PEARL)
        .name(Language.INTERFACE_HOME_BUTTON.toString())
        .lore(Language.INTERFACE_HOME_BUTTON_DESC.getDescriptionArray())
        .build(), e -> {
      region.setFlag(Flags.TELE_LOC, BukkitAdapter.adapt(player.getLocation()));
      region.setDirty(true);
      player.sendMessage(Language.INTERFACE_HOME_BUTTON_SUCCESS.toChatString());
    }));

    contents.set(0, 4, ClickableItem.empty(new ItemBuilder(Material.BOOK).name(Language.INTERFACE_MANAGE_BUTTON_INFO.toString())
        .lore(Language.INTERFACE_MANAGE_BUTTON_INFO_DESCRIPTION.getDescriptionArray())
        .enchantment(Enchantment.ARROW_INFINITE)
        .setItemFlag(ItemFlag.HIDE_ENCHANTS)
        .build()));

    if (player.hasPermission("region.manage.delregion"))
      contents.set(0, 8, ClickableItem.of(new ItemBuilder(Material.TNT)
          .name(Language.INTERFACE_MANAGE_BUTTON_DELETEREGION.toString())
          .lore(Language.INTERFACE_MANAGE_BUTTON_DELETEREGION_DESCRIPTION.getDescriptionArray())
          .build(), e -> SmartInventory.builder().provider(new RegionDeleteConfirm(claim))
          .title(Language.INTERFACE_DELETE_TITLE.toString())
          .size(1)
          .build().open(player)));

    if (player.hasPermission("region.manage.flagmenu"))
      contents.set(1, 5, ClickableItem.of(new ItemBuilder(Material.COMMAND_BLOCK)
              .name(Language.INTERFACE_MANAGE_BUTTON_FLAG.toString()).build(),
          e -> {
            SmartInventory.builder()
                .provider(new RegionFlagMenu(claim))
                .size(5, 9)
                .title(Language.FLAG_TITLE.toString()).build().open(player);

          }));

    contents.set(1, 7, ClickableItem.of(new ItemBuilder(Material.EXPERIENCE_BOTTLE)
        .name(Language.INTERFACE_MANAGE_BUTTON_SHOWBORDER.toString())
        .lore(Language.INTERFACE_MANAGE_BUTTON_SHOWBORDER_DESCRIPTION.getDescriptionArray())
        .build(), e -> new BorderDisplay(player, claim)));
  }
}