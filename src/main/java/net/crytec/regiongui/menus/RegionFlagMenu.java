package net.crytec.regiongui.menus;

import com.google.common.collect.Sets;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import java.util.LinkedHashSet;
import net.crytec.inventoryapi.SmartInventory;
import net.crytec.inventoryapi.api.ClickableItem;
import net.crytec.inventoryapi.api.InventoryContent;
import net.crytec.inventoryapi.api.InventoryProvider;
import net.crytec.inventoryapi.api.Pagination;
import net.crytec.inventoryapi.api.SlotIterator;
import net.crytec.inventoryapi.api.SlotIterator.Type;
import net.crytec.inventoryapi.api.SlotPos;
import net.crytec.libs.commons.utils.item.ItemBuilder;
import net.crytec.regiongui.Language;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.data.ClaimEntry;
import net.crytec.regiongui.data.flags.FlagSetting;
import net.crytec.regiongui.manager.FlagManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class RegionFlagMenu implements InventoryProvider {

  private static final ItemStack fill = new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").build();
  private final FlagManager flagManager;
  private final ClaimEntry claim;


  public RegionFlagMenu(final ClaimEntry claim) {
    this.claim = claim;
    flagManager = RegionGUI.getInstance().getFlagManager();
  }

  @Override
  public void init(final Player player, final InventoryContent contents) {

    contents.fillRow(0, ClickableItem.empty(fill));
    contents.fillRow(4, ClickableItem.empty(fill));

    final ProtectedRegion region = claim.getProtectedRegion().get();

    final Pagination pagination = contents.pagination();
    final LinkedHashSet<ClickableItem> items = Sets.newLinkedHashSet();

    for (final FlagSetting flag : flagManager.getActiveFlags())
      if (player.hasPermission(flag.getPermission()) || player.hasPermission("region.flagmenu.all")) {
        items.add(flag.getButton(player, region, contents));
      }

    ClickableItem[] c = new ClickableItem[items.size()];
    c = items.toArray(c);

    SlotIterator slotIterator = contents.newIterator(Type.HORIZONTAL, SlotPos.of(0, 0));
    slotIterator = slotIterator.allowOverride(false);

    pagination.setItems(c);
    pagination.setItemsPerPage(27);
    pagination.addToIterator(slotIterator);

    contents.set(4, 4, ClickableItem.of(new ItemBuilder(Material.RED_WOOL).name(Language.INTERFACE_BACK.toString()).build(), e -> {
      SmartInventory.builder().provider(new RegionManageInterface(claim)).size(3).title(Language.INTERFACE_MANAGE_TITLE.toString()).build().open(player);
    }));

    if (!pagination.isLast())
      contents.set(4, 6, ClickableItem.of(new ItemBuilder(Material.MAP).name(Language.INTERFACE_NEXT_PAGE.toString()).build(), e -> {
        contents.getHost().open(player, pagination.next().getPage());
      }));

    if (!pagination.isFirst())
      contents.set(4, 2, ClickableItem.of(new ItemBuilder(Material.MAP).name(Language.INTERFACE_PREVIOUS_PAGE.toString()).build(), e -> {
        contents.getHost().open(player, pagination.previous().getPage());
      }));
  }
}