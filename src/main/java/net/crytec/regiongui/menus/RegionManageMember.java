package net.crytec.regiongui.menus;

import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.domains.PlayerDomain;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.util.profile.Profile;
import java.util.ArrayList;
import java.util.UUID;
import net.crytec.inventoryapi.SmartInventory;
import net.crytec.inventoryapi.api.ClickableItem;
import net.crytec.inventoryapi.api.InventoryContent;
import net.crytec.inventoryapi.api.InventoryProvider;
import net.crytec.inventoryapi.api.Pagination;
import net.crytec.inventoryapi.api.SlotIterator;
import net.crytec.inventoryapi.api.SlotIterator.Type;
import net.crytec.inventoryapi.api.SlotPos;
import net.crytec.libs.commons.utils.UtilPlayer;
import net.crytec.libs.commons.utils.item.ItemBuilder;
import net.crytec.regiongui.Language;
import net.crytec.regiongui.data.ClaimEntry;
import net.crytec.regiongui.events.RegionAddMemberEvent;
import net.crytec.regiongui.events.RegionRemoveMemberEvent;
import net.crytec.regiongui.util.PlayerChatInput;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class RegionManageMember implements InventoryProvider {

  private static final ItemStack fill = new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").build();

  private final ClaimEntry claim;

  public RegionManageMember(final ClaimEntry claim) {
    this.claim = claim;
  }

  @Override
  public void init(final Player player, final InventoryContent contents) {

    contents.fillBorders(ClickableItem.empty(fill));

    final ProtectedRegion region = claim.getProtectedRegion().get();

    final Pagination pagination = contents.pagination();
    final ArrayList<ClickableItem> items = new ArrayList<>();

    for (final UUID uuid : region.getMembers().getUniqueIds()) {
      final OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(uuid);

      final String name = offlinePlayer.hasPlayedBefore() ? offlinePlayer.getName() : "Unknown Name";

      final ItemStack item = new ItemBuilder(Material.PLAYER_HEAD).name(ChatColor.WHITE + name).lore(Language.INTERFACE_REMOVE_DESC.toString().replaceAll("%name%", name)).build();

      if (offlinePlayer.hasPlayedBefore()) {
        final ItemStack skull = item;
        final SkullMeta sm = (SkullMeta) skull.getItemMeta();
        sm.hasOwner();
        sm.setOwningPlayer(offlinePlayer);
        skull.setItemMeta(sm);
      }

      items.add(ClickableItem.of(item, e -> {

        final RegionRemoveMemberEvent event = new RegionRemoveMemberEvent(player, claim.getTemplate(), uuid);
        Bukkit.getPluginManager().callEvent(event);

        if (event.isCancelled()) {
          contents.getHost().open(player);
          return;
        }

        region.getMembers().removePlayer(uuid);
        contents.getHost().open(player, pagination.getPage());
        UtilPlayer.playSound(player, Sound.BLOCK_LEVER_CLICK);
        player.sendMessage(Language.INTERFACE_REMOVE_SUCESSFULL.toChatString().replaceAll("%name%", name));
      }));
    }

    ClickableItem[] c = new ClickableItem[items.size()];
    c = items.toArray(c);

    pagination.setItems(c);
    pagination.setItemsPerPage(18);

    contents.set(SlotPos.of(0, 4), ClickableItem.of(new ItemBuilder(Material.WRITABLE_BOOK)
        .name(Language.INTERFACE_MANAGE_BUTTON_ADDMEMBER.toString()).build(), e -> {
      player.closeInventory();
      player.sendMessage(Language.REGION_MESSAGE_CHATADDMEMBER.toChatString());

      PlayerChatInput.get(player, n -> {

        final OfflinePlayer op = Bukkit.getOfflinePlayer(n);

        if (!op.hasPlayedBefore()) {
          player.sendMessage(Language.ERROR_INVALID_OFFLINEPLAYER.toChatString());
          return;
        }

        final RegionAddMemberEvent event = new RegionAddMemberEvent(player, claim.getTemplate(), op.getUniqueId());
        Bukkit.getPluginManager().callEvent(event);

        if (event.isCancelled()) {
          reopen(player, contents);
          return;
        }

        final DefaultDomain domain = region.getMembers();
        final PlayerDomain playerDomain = domain.getPlayerDomain();

        playerDomain.addPlayer(op.getUniqueId());

        domain.setPlayerDomain(playerDomain);

        region.setMembers(domain);
        region.setDirty(true);

        final Profile profile = new Profile(op.getUniqueId(), op.getName());
        WorldGuard.getInstance().getProfileCache().put(profile);

        domain.toUserFriendlyComponent(WorldGuard.getInstance().getProfileCache());
        reopen(player, contents);
      });

    }));

    contents.set(4, 4, ClickableItem.of(new ItemBuilder(Material.RED_WOOL).name(Language.INTERFACE_BACK.toString()).build(), e -> {
      SmartInventory.builder().provider(new RegionManageInterface(claim)).size(3).title(Language.INTERFACE_MANAGE_TITLE.toString()).build().open(player);
    }));

    contents.set(4, 6, ClickableItem.of(new ItemBuilder(Material.MAP).name(Language.INTERFACE_NEXT_PAGE.toString()).build(), e -> {
      contents.getHost().open(player, pagination.next().getPage());
    }));
    contents.set(4, 2, ClickableItem.of(new ItemBuilder(Material.MAP).name(Language.INTERFACE_PREVIOUS_PAGE.toString()).build(), e -> {
      contents.getHost().open(player, pagination.previous().getPage());
    }));

    SlotIterator slotIterator = contents.newIterator(Type.HORIZONTAL, SlotPos.of(1, 1));
    slotIterator = slotIterator.allowOverride(false);
    pagination.addToIterator(slotIterator);
  }
}