package net.crytec.regiongui.menus;

import java.util.ArrayList;
import java.util.List;
import net.crytec.inventoryapi.SmartInventory;
import net.crytec.inventoryapi.api.ClickableItem;
import net.crytec.inventoryapi.api.InventoryContent;
import net.crytec.inventoryapi.api.InventoryProvider;
import net.crytec.inventoryapi.api.Pagination;
import net.crytec.inventoryapi.api.SlotIterator.Type;
import net.crytec.inventoryapi.api.SlotPos;
import net.crytec.libs.commons.utils.item.ItemBuilder;
import net.crytec.regiongui.Language;
import net.crytec.regiongui.data.ClaimEntry;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class RegionSelectMenu implements InventoryProvider {

  private final List<ClaimEntry> claims;

  public RegionSelectMenu(final List<ClaimEntry> claims) {
    this.claims = claims;
  }

  @Override
  public void init(final Player player, final InventoryContent contents) {

    final Pagination pagination = contents.pagination();
    final ArrayList<ClickableItem> items = new ArrayList<>();

    for (final ClaimEntry claim : claims) {
      final String id = ChatColor.GREEN + claim.getRegionID();
      final String lore = Language.INTERFACE_SELECT_DESCRIPTION.toString().replace("%region%", id);

      final ItemStack item = new ItemBuilder(Material.BOOK).name(id).lore(lore).build();
      items.add(ClickableItem.of(item, e -> {
        SmartInventory.builder().provider(new RegionManageInterface(claim)).size(3).title(Language.INTERFACE_MANAGE_TITLE.toString()).build().open(player);
      }));
    }

    ClickableItem[] c = new ClickableItem[items.size()];
    c = items.toArray(c);

    pagination.setItems(c);
    pagination.setItemsPerPage(18);
    pagination.addToIterator(contents.newIterator(Type.HORIZONTAL, SlotPos.of(0, 1)));
  }
}