package net.crytec.regiongui.menus;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.crytec.inventoryapi.SmartInventory;
import net.crytec.inventoryapi.api.ClickableItem;
import net.crytec.inventoryapi.api.InventoryContent;
import net.crytec.inventoryapi.api.InventoryProvider;
import net.crytec.libs.commons.utils.item.ItemBuilder;
import net.crytec.regiongui.Language;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.data.ClaimEntry;
import net.crytec.regiongui.util.RegionUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class RegionDeleteConfirm implements InventoryProvider {

  private static final ItemStack fill = new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").build();

  private final ClaimEntry claim;

  public RegionDeleteConfirm(final ClaimEntry claim) {
    this.claim = claim;
  }

  @Override
  public void init(final Player player, final InventoryContent contents) {

    contents.fill(ClickableItem.empty(fill));

    if (!claim.getProtectedRegion().isPresent()) {
      System.out.println("Claim not present");
      player.closeInventory();
      return;
    }

    final ProtectedRegion region = claim.getProtectedRegion().get();

    contents.set(0, 2, ClickableItem.of(new ItemBuilder(Material.REDSTONE).name(Language.INTERFACE_DELETE_ABORT_BUTTON.toString()).build(), e -> {
      SmartInventory.builder().provider(new RegionManageInterface(claim)).size(3).title(Language.INTERFACE_MANAGE_TITLE.toString()).build().open(player);
    }));
    contents.set(0, 6, ClickableItem.of(new ItemBuilder(Material.EMERALD).name(Language.INTERFACE_DELETE_CONFIRM_BUTTON.toString()).build(), e -> {

      player.closeInventory();

      RegionGUI.getInstance().getPlayerManager().deleteClaim(player.getUniqueId(), region);

      RegionUtils.getRegionManager(player.getWorld()).removeRegion(region.getId());
      RegionUtils.saveRegions(player.getWorld());

      if (claim.getTemplate().hasRefund()) {
        RegionGUI.getInstance().getEconomy().depositPlayer(player, claim.getTemplate().getRefund());
        player.sendMessage(Language.REGION_REMOVED_REFUNDED.toString().replaceAll("%refund%", String.valueOf(claim.getTemplate().getRefund())));
      }

      player.sendMessage(Language.REGION_REMOVED.toChatString());
    }));
  }
}