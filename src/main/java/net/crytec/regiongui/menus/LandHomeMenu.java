package net.crytec.regiongui.menus;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.util.Location;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import java.util.ArrayList;
import java.util.List;
import net.crytec.inventoryapi.api.ClickableItem;
import net.crytec.inventoryapi.api.InventoryContent;
import net.crytec.inventoryapi.api.InventoryProvider;
import net.crytec.inventoryapi.api.Pagination;
import net.crytec.inventoryapi.api.SlotIterator;
import net.crytec.inventoryapi.api.SlotIterator.Type;
import net.crytec.inventoryapi.api.SlotPos;
import net.crytec.libs.commons.utils.item.ItemBuilder;
import net.crytec.regiongui.Language;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.data.ClaimEntry;
import net.crytec.regiongui.util.RegionUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class LandHomeMenu implements InventoryProvider {

  private static final ItemStack fill = new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).name(" ").build();


  @Override
  public void init(final Player player, final InventoryContent contents) {
    contents.fillBorders(ClickableItem.empty(fill));

    final Pagination pagination = contents.pagination();
    final ArrayList<ClickableItem> items = new ArrayList<>();

    for (final ClaimEntry claim : RegionGUI.getInstance().getPlayerManager().getPlayerClaims(player.getUniqueId())) {

      final World world = claim.getTemplate().getWorld().get();
      final RegionManager rm = RegionUtils.getRegionManager(world);

      final ProtectedRegion region = rm.getRegion(claim.getRegionID());
      if (region == null) {
        Bukkit.getLogger().severe("WorldGuard region is missing [" + claim.getRegionID() + "]. This region is still assigned to the player but missing in WorldGuard!");
        Bukkit.getLogger().severe("world: " + world.getName() + " template id: " + claim.getTemplate());
        continue;
      }

      final List<String> desc = new ArrayList<>(Language.INTERFACE_HOME_ENTRYBUTTON_DESCRIPTION.getDescriptionArray());
      desc.replaceAll(line -> line.replace("%world%", world.getName()));

      items.add(ClickableItem.of(new ItemBuilder(Material.GRAY_BED).name(claim.getRegionID()).lore(desc).build(), e -> {
        if (region.getFlag(Flags.TELE_LOC) != null) {

          final Location loc = region.getFlag(Flags.TELE_LOC);
          final org.bukkit.Location l = BukkitAdapter.adapt(loc);
          player.teleport(l);
        } else
          player.sendMessage(Language.ERROR_NO_HOME_SET.toChatString());
      }));
    }

    ClickableItem[] c = new ClickableItem[items.size()];
    c = items.toArray(c);

    pagination.setItems(c);
    pagination.setItemsPerPage(27);

    if (!pagination.isLast())
      contents.set(4, 6, ClickableItem.of(new ItemBuilder(Material.MAP).name(Language.INTERFACE_NEXT_PAGE.toString()).build(), e -> {
        contents.getHost().open(player, pagination.next().getPage());
      }));

    if (!pagination.isFirst())
      contents.set(4, 2, ClickableItem.of(new ItemBuilder(Material.MAP).name(Language.INTERFACE_PREVIOUS_PAGE.toString()).build(), e -> {
        contents.getHost().open(player, pagination.previous().getPage());
      }));

    SlotIterator slotIterator = contents.newIterator(Type.HORIZONTAL, SlotPos.of(1, 1));
    slotIterator = slotIterator.allowOverride(false);
    pagination.addToIterator(slotIterator);


  }
}