package net.crytec.regiongui.listener;

import net.crytec.libs.commons.utils.lang.StringUtils;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.data.RegionClaim;
import net.crytec.regiongui.events.RegionPurchasedEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class InternalListeners implements Listener {

  private final RegionGUI plugin;

  public InternalListeners(final RegionGUI plugin) {
    this.plugin = plugin;
  }

  @EventHandler
  public void onRegionPurchase(final RegionPurchasedEvent event) {
    final RegionClaim claim = event.getRegionClaim();
    final Player p = event.getPlayer();

    if (claim.getRunCommands().isEmpty()) {
      return;
    }

    for (final String command : claim.getRunCommands()) {
      String cmd = command.replace("%player%", p.getName())
          .replace("%region%", event.getProtectedRegion().getId())
          .replace("%world%", p.getWorld().getName());

      if (cmd.startsWith("<server>")) {

        cmd = cmd.replace("<server>", "");
        cmd = StringUtils.trim(cmd);

        RegionGUI.getInstance().getLogger().info("Performing Command:" + cmd);
        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), cmd);
      } else {
        p.performCommand(cmd);
      }
    }
  }
}