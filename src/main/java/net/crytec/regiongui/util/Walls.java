package net.crytec.regiongui.util;

import com.google.common.collect.Sets;
import com.sk89q.worldedit.math.BlockVector2;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Queue;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.data.RegionClaim;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.scheduler.BukkitTask;

public class Walls implements Runnable {

  private final World world;
  private final Material material;
  private final BukkitTask task;
  private final Queue<BlockVector2> toFill = new ArrayDeque<>();

  private static final int increment = 10;

  public Walls(final RegionClaim claim, final ProtectedRegion region) {
    final CuboidRegion region1 = new CuboidRegion(region.getMaximumPoint(), region.getMinimumPoint());

    region1.setPos1(region.getMaximumPoint().withY(0));
    region1.setPos2(region.getMinimumPoint().withY(0));

    final HashSet<BlockVector2> temp = Sets.newHashSet();
    region1.getWalls().forEach(vec -> temp.add(vec.toBlockVector2()));
    toFill.addAll(temp);

    world = claim.getWorld().get();
    material = claim.getBorderMaterial();

    task = Bukkit.getScheduler().runTaskTimer(RegionGUI.getInstance(), this, 5, 10);
  }

  @Override
  public void run() {
    for (int i = 0; i <= increment; i++) {

      if (toFill.isEmpty()) {
        task.cancel();
        return;
      }

      final BlockVector2 vec = toFill.poll();
      final Block block = getHighestBlock(world, vec.getBlockX(), vec.getBlockZ());

      block.setType(material, true);
      world.playEffect(block.getLocation(), Effect.STEP_SOUND, material);
    }
  }


  private Block getHighestBlock(final World world, final int x, final int z) {
    Block b = world.getHighestBlockAt(x, z);

    while (Tag.LEAVES.isTagged(b.getType()) || b.getType() == Material.AIR || b.getType() == Material.GRASS || b.getType() == Material.TALL_GRASS) {
      b = b.getRelative(BlockFace.DOWN);
    }
    return b.getRelative(BlockFace.UP);
  }
}