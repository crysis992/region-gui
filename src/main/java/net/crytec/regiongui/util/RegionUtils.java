package net.crytec.regiongui.util;

import com.google.common.collect.Lists;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.internal.platform.WorldGuardPlatform;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.managers.storage.StorageException;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import net.crytec.libs.commons.utils.lang.EnumUtils;
import net.crytec.regiongui.Language;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.data.ClaimEntry;
import net.crytec.regiongui.manager.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class RegionUtils {

  private static final WorldGuardPlatform platform;
  private static final PlayerManager manager;

  static {
    platform = WorldGuard.getInstance().getPlatform();
    manager = RegionGUI.getInstance().getPlayerManager();
  }

  public static boolean validateMaterial(final String material) {
    return EnumUtils.isValidEnum(Material.class, material);
  }

  public static boolean saveRegions(final World world) {
    try {
      platform.getRegionContainer().get(BukkitAdapter.adapt(world)).saveChanges();
      return true;
    } catch (final StorageException e) {
      e.printStackTrace();
      return false;
    }
  }

  public static RegionManager getRegionManager(final World world) {
    return platform.getRegionContainer().get(BukkitAdapter.adapt(world));
  }

  public static List<Vector> getLocationsFromRegion(final ProtectedRegion region) {

    final BlockVector3 min = region.getMinimumPoint();
    final BlockVector3 max = region.getMaximumPoint();
    final int height = (int) region.getMaximumPoint().getY() - (int) region.getMinimumPoint().getY();

    final List<Vector> locs = new ArrayList<>();

    final List<Vector> bC = new ArrayList<>();

    bC.add(new Vector(min.getX(), min.getY(), min.getZ())); // Bottom
    // Corners
    bC.add(new Vector(max.getX(), min.getY(), min.getZ())); // Bottom
    // Corners
    bC.add(new Vector(max.getX(), min.getY(), max.getZ())); // Bottom
    // Corners
    bC.add(new Vector(min.getX(), min.getY(), max.getZ())); // Bottom
    // Corners

    for (int i = 0; i < bC.size(); i++) {
      final Vector p1 = bC.get(i);
      final Vector p2;
      Vector p5;
      if (i + 1 < bC.size()) {
        p2 = (Vector) bC.get(i + 1);
      } else {
        p2 = (Vector) bC.get(0);
      }

      final Vector p3 = p1.add(new Vector(0, height, 0));
      final Vector p4 = p2.add(new Vector(0, height, 0));
      locs.addAll(regionLine(p1, p2));
      locs.addAll(regionLine(p3, p4));
      locs.addAll(regionLine(p1, p3));

      for (double offset = 2.0; offset < height; offset += 2.0) {
        p5 = p1.add(new Vector(0, offset, 0));
        final Vector p6 = p2.add(new Vector(0, offset, 0));
        locs.addAll(regionLine(p5, p6));
      }
    }
    return locs;
  }

  public static List<Vector> regionLine(final Vector p1, final Vector p2) {
    final List<Vector> locs = new ArrayList<>();
    final int points = (int) (p1.distance(p2) / 1.0) + 1;

    final double length = p1.distance(p2);
    final double gap = length / (points - 1);

    final Vector gapVector = p2.subtract(p1).normalize().multiply(gap);
    for (int i = 0; i < points; i++) {
      final Vector currentPoint = p1.add(gapVector.multiply(i));
      locs.add(currentPoint);
    }
    return locs;
  }

  public static List<ClaimEntry> getRegionsAtPosition(final Player owner) {
    final List<ClaimEntry> result = Lists.newArrayList();

    final RegionManager rm = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(owner.getWorld()));
    if (rm == null) {
      Bukkit.getLogger().severe("Unable to access WorldGuard for world " + owner.getWorld().getName());
      return result;
    }
    final ApplicableRegionSet ar = rm.getApplicableRegions(BukkitAdapter.asBlockVector(owner.getLocation()));
    final LocalPlayer lp = WorldGuardPlugin.inst().wrapPlayer(owner);

    //All regions at current position
    final Iterator<ProtectedRegion> iter = ar.iterator();
    final Set<String> regionIds = manager.getPlayerClaims(owner.getUniqueId()).stream().map(ClaimEntry::getRegionID).collect(Collectors.toSet());

    if (ar.size() == 0) {
      //Return empty list if there are no regions at the current position.
      return result;
    } else if (ar.size() == 1) {
      final ProtectedRegion region = iter.next();

      if (region.isOwner(lp) && regionIds.contains(region.getId()) || owner.hasPermission("region.mod")) {

        final Optional<ClaimEntry> claim = manager.getPlayerClaims(owner.getUniqueId()).stream()
            .filter(entry -> entry.getRegionID().equals(region.getId()))
            .findFirst();

        if (!claim.isPresent()) {
          owner.sendMessage(Language.ERROR_NO_REGION_FOUND.toChatString());
          return result;
        }
        result.add(claim.get());
      }
      return result;
    } else {
      //Multiple Regions found at the current position

      while (iter.hasNext()) {
        final ProtectedRegion region = iter.next();
        if (region.isOwner(lp) && regionIds.contains(region.getId()) || owner.hasPermission("region.mod")) {

          final Optional<ClaimEntry> claim = manager.getPlayerClaims(owner.getUniqueId()).stream()
              .filter(entry -> entry.getRegionID().equals(region.getId()))
              .findFirst();

          if (claim.isPresent()) {
            result.add(claim.get());
          }
        }
      }
      return result;
    }
  }

//  public static List<ClaimEntry> getRegionsAtPosition(final Player player, final PlayerManager manager) {
//    final RegionManager rm = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(player.getWorld()));
//    final ApplicableRegionSet ar = rm.getApplicableRegions(BukkitAdapter.asBlockVector(player.getLocation()));
//    final LocalPlayer lp = WorldGuardPlugin.inst().wrapPlayer(player);
//
//    final Iterator<ProtectedRegion> iter = ar.iterator();
//
//    if (ar.size() == 0) {
//      return Lists.newArrayList();
//    } else if (ar.size() == 1) {
//      final ProtectedRegion pr = iter.next();
//
//      final Set<ClaimEntry> claims = manager.getPlayerClaims(player.getUniqueId());
//      if (claims == null) {
//        player.sendMessage(ChatColor.RED + "You don't own any claims in this world.");
//        return Lists.newArrayList();
//      }
//
//      final Set<String> regionIds = claims.stream().map(ClaimEntry::getRegionID).collect(Collectors.toSet());
//
//      if ((pr.isOwner(lp) && regionIds.contains(pr.getId())) || player.hasPermission("region.mod")) {
//
//        final Optional<ClaimEntry> claim = manager.getPlayerClaims(player.getUniqueId()).stream()
//            .filter(entry -> entry.getRegionID().equals(pr.getId()))
//            .findFirst();
//
//        if (!claim.isPresent()) {
//          player.sendMessage(Language.ERROR_NO_REGION_FOUND.toChatString());
//          return Lists.newArrayList();
//        }
//        return Lists.newArrayList(claim.get());
//      } else {
//        player.sendMessage(Language.ERROR_NOT_OWNER.toChatString());
//        return Lists.newArrayList();
//      }
//    } else {
//      final Set<String> regionIds = manager.getPlayerClaims(player.getUniqueId()).stream().map(ClaimEntry::getRegionID).collect(Collectors.toSet());
//      return manager.getPlayerClaims(player.getUniqueId()).stream().filter(claim -> regionIds.contains(claim.getRegionID())).collect(Collectors.toList());
//    }
//  }
}