package net.crytec.regiongui.util;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.domains.PlayerDomain;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.util.profile.Profile;
import net.crytec.regiongui.Language;
import net.crytec.regiongui.RegionGUI;
import net.crytec.regiongui.data.RegionClaim;
import net.crytec.regiongui.events.RegionPurchasedEvent;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

public class PlotBuilder {

  private final RegionGUI plugin;
  private final Player player;
  private final String displayname;
  private final RegionClaim claim;
  private final Location loc;
  private final RegionManager manager;
  private final LocalPlayer localPlayer;

  public PlotBuilder(final Player player, final String displayname, final RegionClaim claim) {
    plugin = JavaPlugin.getPlugin(RegionGUI.class);
    this.player = player;
    this.displayname = displayname;
    this.claim = claim;
    loc = player.getLocation();

    manager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(player.getWorld()));
    localPlayer = WorldGuardPlugin.inst().wrapPlayer(player);
  }

  public void build() {
    final int x = loc.getBlockX();
    final int z = loc.getBlockZ();
    final int y = loc.getBlockY();
    final int size = claim.getSize();

    if (claim.getPermission() != null && !claim.getPermission().isEmpty()) {
      if (!player.hasPermission(claim.getPermission())) {
        player.sendMessage(Language.ERROR_NO_PERMISSION.toChatString().replaceAll("%permission%", claim.getPermission()));
        return;
      }
    }

    final int half_size = (int) Math.round(size / 2.0D); // Claim size fix

    final Vector pointUp = new Vector(x + half_size, y + claim.getHeight(), z + half_size);
    final Vector pointDown = new Vector(x - half_size, y - claim.getDepth(), z - half_size);

    final BlockVector3 p1 = BlockVector3.at(pointUp.getBlockX(), pointUp.getBlockY(), pointUp.getBlockZ());
    final BlockVector3 p2 = BlockVector3.at(pointDown.getBlockX(), pointDown.getBlockY(), pointDown.getBlockZ());

    final String identifier = plugin.getConfig().getString("region-identifier").replace("%player%", player.getName()).replace("%displayname%", displayname);
    final ProtectedRegion pr = new ProtectedCuboidRegion(identifier, p1, p2);

    if (manager.overlapsUnownedRegion(pr, localPlayer) && plugin.getConfig().getBoolean("preventRegionOverlap", true)) {
      player.sendMessage(Language.ERROR_OVERLAP.toString());
      return;
    }

    final int price = claim.getPrice();

    final DefaultDomain domain = pr.getOwners();
    final PlayerDomain playerDomain = domain.getPlayerDomain();

    final Profile profile = new Profile(player.getUniqueId(), player.getName());
    WorldGuard.getInstance().getProfileCache().put(profile);

    playerDomain.addPlayer(player.getUniqueId());

    domain.setPlayerDomain(playerDomain);
    pr.setOwners(domain);

    pr.setFlag(Flags.TELE_LOC, BukkitAdapter.adapt(player.getLocation()));

    final EconomyResponse r = RegionGUI.getInstance().getEconomy().withdrawPlayer(player.getPlayer(), price);
    if (!r.transactionSuccess()) {
      player.sendMessage(Language.ERROR_NO_MONEY.toChatString());
      return;
    }

    player.sendMessage(Language.REGION_CREATE_MONEY.toChatString().replace("%money%", "" + price).replace("%currencyname%", RegionGUI.getInstance().getEconomy().currencyNameSingular()));
    pr.setDirty(true);

    if (claim.isGenerateBorder()) {
      new Walls(claim, pr);
    }
    manager.addRegion(pr);
    player.sendMessage(Language.REGION_CREATE_SUCCESS.toChatString());

    RegionGUI.getInstance().getPlayerManager().addClaimToPlayer(player.getUniqueId(), pr, claim);

    Bukkit.getScheduler().runTask(RegionGUI.getInstance(), () -> Bukkit.getPluginManager().callEvent(new RegionPurchasedEvent(player, price, claim, pr)));
  }
}