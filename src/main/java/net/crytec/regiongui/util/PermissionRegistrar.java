package net.crytec.regiongui.util;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

public class PermissionRegistrar {


  public static Permission register(final String perm, final String desc, final PermissionDefault def) {
    try {
      Bukkit.getPluginManager().addPermission(new Permission(perm, desc, def));
    } catch (final Exception ignored) {
    }
    final Permission permission = Bukkit.getPluginManager().getPermission(perm);
    Validate.notNull(permission, "Failed to register permission!");
    return permission;
  }
}